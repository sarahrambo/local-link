# Local Link CS Capstone

## Description
Local Link is a location-based mobile app. It allows users to chat and interact with those physically around them. In-app features include chatting and mini-games. Data visualizations show anonymized information. Interactive map shows active users locations and allows for users to start chatting.

_We seek to foster a sense of community in an entertaining manner, through a location-based mobile app._

## Project Structure
```
├── deployment                  React native mobile frontend
│   └── ...
├── locallink-dashboard         React admin dashboard
│   └── ...
└── backend
    ├── deployment              K8s .yaml files, deployment scripts
    │   └── ...
    ├── Makefile                Makefile with some basic helper commands
    ├── ...
    ├── src                     Backend source code directory
    │   ├── app.ts              Main express server code
    │   └── ...
    ├── testing                 Testing scripts
    │   └── ...
    └── tsconfig.json
```


## Running/Accessing the project
- https://locallinkb.discovery.cs.vt.edu - the deployed backend
- https://cloud.cs.vt.edu - Log in to access k8s deployments via Rancher UI
- https://cloud.mongo.com - Log in to browse or manually edit MongoDB collections
- https://locallinkd.discovery.cs.vt.edu - the deployed admin dashboard
- Frontend: needs to be run locally via expo go

## Deployment
To deploy your current development backend, the command `make pushbc` can be run in the `backend` 
directory to containerize the backend and push to our container registry. To restart the k8s 
deployment with the new container, run the script `deployment/pullimagebackend.sh`. The status of 
the k8s deployments and services can be checked at any point by running `deployment/getstatus.sh`.

To deploy the react dashboard, comparable steps can be taken. 

1. `cd locallink-dashboard` - Ensure you are in the proper directory
    - Optional: Run `npm run lint` and `npm run build` locally to check for errors
2. `make pushdash` - Dockerize and push container to private registry.
3. `./../backend/deployment/pullimagedash.sh` - Redeploy with new image




# Running Project with Local Backend
- Ensure you have ran `npm install` inside of both frontend/ and backend/ directories
- Turn off Windows Firewall on your laptop to avoid any potential Expo Go errors when running on your device.
- From the top directory, open two terminals:

## Frontend
1. `cd` into the frontend directory
2. In `constants.js`, ensure that the proper backend URL is uncommented.
3. Verify that `.env.js` is present an properly exports a string `IP_ADDRESS`.
4. Run `npm start`
5. Scan qr code on mobile device. Make sure you have Expo Go installed on your device.

## Backend
1. `cd` into the backend directory
2. Run `npm start`

# Running Project with Deployed Backend
## Frontend
Follow the same procedure as when running the backend locally. (Make sure the uncommented value 
in `constants.js` is correct).

## Backend
If the backend is already deployed, there are no necessary steps to take here.

### Deploying the Backend
Note: the following instructions assume working from a linux environment, with docker installed and 
the docker daemon running (can be started with `sudo systemctl start docker`).

If working from a windows environment, certain adjustments may be required. (ie do not use sudo in 
given commands, should not need to manually start docker).

Next, ensure that you are logged in to our private registry, allowing you to push new containers: 
`sudo docker login container.cs.vt.edu`

With this setup, you should now be able to dockerize and push the current backend to our registry. 
This is exemplified as the command under `make pushbc`. Note that it currently uses tag `0.1`, and 
it will likely be best to change this for significant updates to retain previous working containers.

New container images can be pulled and deployed to the server using the Rancher UI at `cloud.cs.vt.edu`. 
Alternatively, run `deployment/pullimagebackend.sh` to pull the latest image from the registry and deploy it to the server.


### Documentation
Useful links for deploying containerized services:
- https://wiki.cs.vt.edu/index.php/Cloud_Quickstart
- https://wiki.cs.vt.edu/index.php/Cloud_Example_Jupyter
- https://wiki.cs.vt.edu/index.php/Howto::Docker_Registry

## Authors and Acknowledgment
This project was created as a capstone project for Virginia Tech's Master of Engineering in Computer Science program. We'd like to express our gratitude to the teaching staff of this course, especially to Dr. Sally Hamouda for a great semester.

This project was made possible through the collaboration of
- Mark Hamilton ​
- Elliot Jimeno ​
- Joseph Mecherikunnel  ​
- Sarah Ramboyong​
- Jonathan West
