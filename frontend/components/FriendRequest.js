// FriendRequest.js
import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

const FriendRequest = ({ request, onAccept, onReject }) => {
  return (
    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10, backgroundColor: "#ffcc00", borderRadius: 5, border: 1, margin: 2 }}>
      <Image
        source={require('../assets/defaultprofile.png')} // Assuming there's no profile picture for friend requests
        style={{ width: 50, height: 50, borderRadius: 25, marginRight: 10 }}
      />
      <View style={{ flex: 1 }}>
        <Text style={{ fontSize: 18 }}>{request.senderName}</Text>
        <Text>Friend Request</Text>
      </View>
      <TouchableOpacity onPress={() => onAccept(request.id)}>
        <Text style={{ color: 'green', marginRight: 10 }}>Accept</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => onReject(request.id)}>
        <Text style={{ color: 'red' }}>Reject</Text>
      </TouchableOpacity>
    </View>
  );
};

export default FriendRequest;
