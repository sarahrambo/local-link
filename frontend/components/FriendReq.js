import { StyleSheet, Button, Text, View, Pressable, Alert, Image } from "react-native";
import React, { useContext } from "react";
import { useNavigation } from "@react-navigation/native";
import { BACKEND_URL } from '../constants';

const styles = StyleSheet.create({
    frListing: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        padding: 10,
        borderTopWidth: 2,
        borderColor: "#cccccc",
    },
    profileImg: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 5,
    },
});

const FriendReq = ({ uid, reqId, prospect, outgoing, responseCallback }) => {
    const navigation = useNavigation();

    let imgSource;
    if (prospect.profile_uri === "") {
        imgSource = require('../assets/defaultprofile.png');
    } else {
        imgSource = { uri: prospect.profile_uri };
    }

    const deleteRequest = async () => {
        try {
            const response = await fetch(`${BACKEND_URL}friend/delete/${reqId}`,
                {
                    method: "DELETE",
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
            );
            if (response.ok) {
                // // Attempt to refresh page
                // navigation.navigate("Comrades");
                responseCallback(false); // false for deleted, true for accepted
            } else {
                console.log("request delete failed", response.status);
            }
        } catch (err) {
            console.log("Friend request delete failed", err);
        }
    };
    const acceptRequest = async () => {
        try {
            const response = await fetch(`${BACKEND_URL}friend/accept/${reqId}`,
                {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
            );
            if (response.ok) {
                responseCallback(true); // false for deleted, true for accepted
            } else {
                console.log("request accept failed", response.status);
            }
        } catch (err) {
            console.log("Friend request accept failed", err);
        }
    };
    const showConfirmDialog = () => {
        return Alert.alert(
            "Are you sure?",
            "Delete friend request?",
            [
                {
                    text: "Yes",
                    onPress: () => {
                        deleteRequest();
                    },
                },
                {
                    text: "No", // Just dismisses dialog
                },
            ]
        );
    };
    return (
        <Pressable style={styles.frListing}>
            <Image style={styles.profileImg} source={imgSource}/>
            <View >
                <Text style={{ fontSize: 15, fontWeight: "bold", marginLeft: 6, flex: 1 }}>
                    {prospect?.name}
                </Text>
                <Text style={{ fontSize: 12, marginLeft: 6, flex: 1 }}>
                    {outgoing ? "Pending friend request" : "Respond to friend request"}
                </Text>
            </View>
            <Button
                title={outgoing ? "Cancel" : "Delete"}
                color="#ff2222"
                onPress={() => showConfirmDialog() }
            />
            {!outgoing && <Button
                title="Accept"
                onPress={() => acceptRequest()}
            />}
        </Pressable>
    );
};

export default FriendReq;

