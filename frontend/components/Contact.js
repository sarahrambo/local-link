import React from 'react';
import { Alert, Pressable, StyleSheet, View, Text, Image } from 'react-native';

const styles = StyleSheet.create({
    friendListing: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        padding: 10,
        borderTopWidth: 2,
        borderColor: "#cccccc",
    },
    profileImg: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 5,
    },
});

const ContactListItem = ({ friend, navigation }) => {
  let imgSource;
  if (friend.profile_uri === "") {
    imgSource = require('../assets/defaultprofile.png');
  } else {
    imgSource = { uri: friend.profile_uri };
  }
    
    const handlePress = () => {
        // TODO: open chat with clicked friend
        console.log(`starting chat with ${friend?.name}`);
        // Alert.alert("NOT IMPLEMENTED: starting chat with friend");
        console.log("friend id", friend._id);
        navigation.navigate('Chat', { peerid: friend._id, friendChatter: friend });
    };

    return (
        <Pressable style={styles.friendListing} onPress={handlePress}>
            <Image style={styles.profileImg} source={imgSource}/>
            <View style={{ marginLeft: 6, flex: 1, justifyContent: "center" }}>
                <Text style={{ fontSize: 18, fontWeight: "bold", alignItems: "center" }}>
                    {friend?.name}
                </Text>
                <Text style={{ fontSize: 12 }}>
                    {friend.lastMessage===null ? ("...") : 
                        ((friend.lastMessage.senderId===friend._id ? friend.name : "You") 
                            + ": " 
                            + (friend.lastMessage.type==='image' ? "Image" : friend.lastMessage.messageContent)
                        )}
                </Text>
            </View>
            {friend.active && <View style={{ width: 10, height: 10, borderRadius: 5, backgroundColor: 'green' }} />}
        </Pressable>
    );

};

export default ContactListItem;
