import React from 'react';
import { StyleSheet, Alert, Button, View, Text, Image, Pressable } from 'react-native';
import { BACKEND_URL } from '../constants';

const styles = StyleSheet.create({
    userListing: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        padding: 10,
        borderTopWidth: 2,
        borderColor: "#cccccc",
    },
    profileImg: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 5,
    },
});

const UserListItem = ({ selfuid, user, frCallback, navigation }) => {
    let imgSource;
    if (user.profile_uri === "") {
        imgSource = require('../assets/defaultprofile.png');
    } else {
        imgSource = { uri: user.profile_uri };
    }

    const sendRequest = async () => {
        const senderId = selfuid;
        const receiverId = user._id;
        try {
            const response = await fetch(`${BACKEND_URL}friend/sendrequest`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    senderId,
                    receiverId
                })
            });
            if (!response.ok) {
                console.log(response.status);
                const errorData = await response.json();
                throw new Error(errorData.error || 'Failed to send friend request');
            } 
            const responseData = await response.json();
            console.log('Friend request sent', responseData.friendRequest);
            frCallback();
            // TODO: refresh page or otherwise update lists
        } catch (error) {
            console.error('Error sending friend request:', error.message);
        }
    };

    const sendFriendReq = () => {
        Alert.alert(
            `Send ${user?.name} friend request?`, "",
            [
                {
                    text: "Confirm",
                    onPress: () => {
                        sendRequest();
                    },
                },
                {
                    text: "Cancel", // Just dismisses dialog
                },
            ]
        );

    };
    const handlePress = () => {
        // TODO: open chat with clicked user
        console.log(`starting chat with ${user?.name}`);
        // Alert.alert("NOT IMPLEMENTED: starting chat with user");
        navigation.navigate('Chat');  // navigate with no params; random ephemeral chat
    };
    // TODO: fix active user field to be accurate
    return (
        <Pressable style={styles.userListing} onPress={handlePress}>
            <Image style={styles.profileImg} source={imgSource}/>
            <View style={{ flex: 1, justifyContent: "center" }}>
                <Text style={{ fontSize: 18, fontWeight: "bold", marginLeft: 6, alignItems: "center" }}>
                    {user?.name}
                </Text>
            </View>
            <Button
                title="Friend Request"
                onPress={() => sendFriendReq()}
            />
            {user.active && <View style={{ width: 10, height: 10, borderRadius: 5, backgroundColor: 'green' }} />}
        </Pressable>
    );
};

export default UserListItem;
