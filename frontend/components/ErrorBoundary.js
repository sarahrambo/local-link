import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";


class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    return { hasError: true };  }

  componentDidCatch(error, errorInfo) {
    // You can log the error to an error reporting service like Sentry
    console.error('Error:', error, errorInfo);
    }

  render() {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
      errorText: {
        textAlign: "center",
      },
    });
    if (this.state.hasError) {
      return (
        <View style={styles.container}>
          <Text style={styles.errorText}>An error occurred. Sorry!</Text>
        </View>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;

