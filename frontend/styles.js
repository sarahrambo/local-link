export default AppStyles = {
    button: {
        borderRadius: 12,
        marginVertical : 10,
        padding: 10,
        backgroundColor: '#2196F3',
        width: '75%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center'
    }
}
