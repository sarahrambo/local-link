import React, { createContext, useContext, useEffect, useState } from 'react';
import io from 'socket.io-client';
import { BACKEND_URL } from './constants';
import { Text, View, ActivityIndicator } from 'react-native';

const SocketContext = createContext();

export const useSocket = () => useContext(SocketContext);

export const SocketProvider = ({ children }) => {
  const [socket, setSocket] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const newSocket = io.connect(BACKEND_URL);

    newSocket.on('connect', () => {
      setSocket(newSocket);
      setLoading(false); // Set loading to false once the connection is established
    });

    return () => {
        newSocket.close();
    }
  }, []);

  if (loading) {
    // You can render a loading indicator here
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Connecting to server...</Text>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  return (
    <SocketContext.Provider value={socket}>
      {children}
    </SocketContext.Provider>
  );
};
