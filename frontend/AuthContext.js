import React, { createContext, useState, useContext, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const AuthContext = createContext();

const AuthProvider = ({ children }) => {
  const [userToken, setUserToken] = useState(null);

  useEffect(() => {
    // Load token from AsyncStorage on component mount
    const loadToken = async () => {
      try {
        const storedToken = await AsyncStorage.getItem('token');
        if (storedToken) {
          setUserToken(storedToken);
        }
      } catch (error) {
        console.error('Error loading token from AsyncStorage:', error);
      }
    };
    loadToken();
  }, []);

  const authLogin = async (token) => {
    try {
      // Save token to AsyncStorage
      await AsyncStorage.setItem('token', token);
      setUserToken(token);
    } catch (error) {
      console.error('Error saving token to AsyncStorage:', error);
    }
  };

  const authLogout = async () => {
    try {
      // Remove token from AsyncStorage
      await AsyncStorage.removeItem('token');
      setUserToken(null);
    } catch (error) {
      console.error('Error removing token from AsyncStorage:', error);
    }
  };

  return (
    <AuthContext.Provider value={{ userToken, authLogin, authLogout }}>
      {children}
    </AuthContext.Provider>
  );
};

export { AuthProvider, AuthContext };
