import { IP_ADDRESS } from './.env';

// For running backend locally
// const BACKEND_URL = `http://${IP_ADDRESS}:3001/`;
// For running frontend with deployed backend
const BACKEND_URL = `https://locallinkb.discovery.cs.vt.edu/`;
export { BACKEND_URL }
