import 'react-native-gesture-handler';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useContext } from 'react';
import { AuthProvider, AuthContext } from './AuthContext';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Text, View, ActivityIndicator } from 'react-native';
import { SocketProvider } from './SocketContext';
import Login from './screens/login';
import Signup from './screens/signup';
import Home from './screens/home';
import Profile from './screens/profile';
import Chat from './screens/chat';
import Chat2 from './screens/chat2';
import Game from './screens/game';
import Game2 from './screens/game2';
import Contacts from './screens/contacts';
import DataScreen from './screens/datascreen';
import MapScreen from './screens/mapscreen';
import ErrorBoundary from './components/ErrorBoundary';
import * as React from 'react';

// Screen Navigation
const MainTabs = createBottomTabNavigator();

// <MainTabs.Screen name="Contacts" component={Home} 
//     options={{
//         tabBarLabel: "Contacts",
//         tabBarIcon: ({ color, size }) => (
//             <MaterialCommunityIcons name="home" color={color} size={size} />
//         ),
//     }}/>
// <MainTabs.Screen name="Chat" component={Chat}
//     options={{ 
//         tabBarLabel: 'Chat',
//         tabBarIcon: ({ color, size }) => (
//             <MaterialCommunityIcons name="chat-outline" color={color} size={size} />
//         ),
//     }}
// />
function Tabs() {
    return (
        <MainTabs.Navigator screenOptions={{
            headerShown: true,
        }}>
            <MainTabs.Screen name="Contacts" component={Contacts} 
                options={{
                    tabBarLabel: "Contacts",
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="home" color={color} size={size} />
                    ),
                }}/>
            <MainTabs.Screen name="Chat" component={Chat2}
                options={{ 
                    tabBarLabel: 'Chat',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="chat-outline" color={color} size={size} />
                    ),
                }}
            />
            <MainTabs.Screen name="Reaction time" component={Game}
                options={{ 
                    tabBarLabel: 'React',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="timer-sand" color={color} size={size} />
                    ),
                    tabBarVisible: false
                }}
            />
            <MainTabs.Screen name="Speedball" component={Game2}
                options={{ 
                    tabBarLabel: 'Speedball',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="play-speed" color={color} size={size} />
                    ),
                    tabBarVisible: true
                }}
            />
            <MainTabs.Screen name="Map" component={MapScreen}
                options={{ 
                    headerShown: false, 
                    tabBarLabel: 'Map',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="map" color={color} size={size} />
                    ),
                }}
            />
            <MainTabs.Screen name="Data" component={DataScreen} 
                options={{ 
                    tabBarLabel: 'Data',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="database" color={color} size={size} />
                    ),
                }}
            />
            <MainTabs.Screen name="Profile" component={Profile} 
                options={{ 
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="account" color={color} size={size} />
                    ),
                }}
            />
        </MainTabs.Navigator>
    );
}

const RootStack = createNativeStackNavigator();

function SplashScreen() {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Getting token...</Text>
            <ActivityIndicator size="large" />
        </View>
    );
}

const MainScreen = ({ onloadCallback }) => {
    const { userToken } = useContext(AuthContext);
    return (
        <ErrorBoundary>
            <RootStack.Navigator screenOptions={{
                headerShown: false,
            }}>
                {userToken == null ? (
                    <>
                        <RootStack.Screen name="Login" component={Login} />
                        <RootStack.Screen name="Signup" component={Signup} />
                    </>
                ) : (
                        <RootStack.Screen name="Tabs" component={Tabs} />
                    )}
            </RootStack.Navigator>
        </ErrorBoundary>
    );
}
export default function App() {
    const [isLoading, setIsLoading] = React.useState(false); // TODO: remove or make use of loading state

    return (
        <NavigationContainer>
            <AuthProvider>
                <SocketProvider>
                    {isLoading ? (
                        <SplashScreen />
                    ) : (
                            <MainScreen onloadCallback={setIsLoading} />
                        )}
                </SocketProvider>
            </AuthProvider>
        </NavigationContainer>
    );
}
