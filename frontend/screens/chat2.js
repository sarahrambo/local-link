import React, { useState, useEffect, useContext, useRef } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { 
    Text, 
    View, 
    Button, 
    TextInput, 
    Platform, 
    FlatList, 
    TouchableWithoutFeedback, 
    Keyboard, 
    Pressable,
    Image,
    StyleSheet,
    KeyboardAvoidingView,
    Alert,
} from 'react-native';
import { useHeaderHeight } from '@react-navigation/elements';
import { Ionicons, MaterialCommunityIcons, MaterialIcons, Entypo } from "@expo/vector-icons";
import io from 'socket.io-client';
import { BACKEND_URL } from '../constants';
import { AuthContext } from '../AuthContext';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ActivityIndicator } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import * as ImageManipulator from 'expo-image-manipulator';
import { useSocket } from '../SocketContext';

const Chat2 = ({ route, navigation }) => {
    const flatListRef = useRef(null);
    const headerHeight = useHeaderHeight();
    // Shared variables (persistent chat, ephemeral chat)
    const { userToken } = useContext(AuthContext);
    const jsonUserToken = JSON.parse(userToken);
    const _id = jsonUserToken._id;
    const name = jsonUserToken.name;
    const socket = useSocket();
    const [messages, setMessages] = useState([]);
    const [partnerName, setPartnerName] = useState("other");
    const [messageContent, setMessageContent] = useState('');
    const [chatStarted, setChatStarted] = useState(false);
    const [partnerProfile, setPartnerProfile] = useState(require('../assets/defaultprofile.png'));
    const [peerid, setPeerid] = useState('');
    // distinguished vars
    // pchat
    const [pchat, setPchat] = useState(false);
    const [friendChatter, setFriendChatter] = useState({});

    // Persistent chat only
    const [dbChatId, setDBChatId] = useState("");

    // Ephemeral chat only
    const [partnerSocketId, setPartnerSocketId] = useState(null);
    const [ePartnerUid, setEPartnerUid] = useState(undefined);

    useFocusEffect(
        React.useCallback(() => {
            console.log("USE FOCUS EFFECT");
            // // TODO: check if route params have changed; if so restart/reload chat accordingly
            // if(!chatStarted) {
            //     if(pchat) {
            //         console.log("starting chat w friend:", friendChatter);
            //         socket.emit('start-chat', { selfid: _id, friendid: friendChatter._id });
            //     } else {
            //         socket.emit('join-matchmaking', { _id: _id, name: name });
            //     }
            // }
            navigation.setOptions({
                headerLeft: () => (
                    <View style={{ flexDirection: "row", marginLeft: 10 }}>
                        <Pressable onPress={() => navigation.navigate("Contacts")}>
                            <MaterialCommunityIcons name="arrow-left" size={28} color="#0077ff"/>
                        </Pressable>
                    </View>
                ),
                headerRight: () => (
                    <View style={{ flexDirection: "row", marginRight: 10 }}>
                        <Button
                            onPress={() => {
                                // Reset page data
                                setPartnerName("other");
                                setChatStarted(false);
                                setPchat(false);
                                setMessages([]);
                                setMessageContent('');
                                navigation.setOptions({ title: "Chat" });
                                setFriendChatter({});
                                if(route.params!==undefined) {
                                    route.params.friendChatter = undefined;
                                    route.params.peerid = undefined;
                                }

                                // start ephemeral chat
                                console.log("starting ephemeral chat");
                                socket.emit('join-matchmaking', { _id: _id, name: name });

                                // Listen for 'chatStarted' event from the backend
                                socket.on('chatStarted', (chatInfo) => {
                                    console.log("starting chat", chatInfo);
                                    setPartnerSocketId(chatInfo.partner.sid);
                                    setPartnerName(chatInfo.partner.name);
                                    setEPartnerUid(chatInfo.partner.uid);
                                    setChatStarted(true);
                                    navigation.setOptions({ title: chatInfo.partner.name });
                                });
                                setPartnerProfile(require('../assets/defaultprofile.png'));
                            }}
                            title="New chat"
                        />
                    </View>
                ),
            });
            console.log('routeparams', route.params);
            if(route.params!==undefined && route.params.peerid!==undefined && route.params.peerid!=="") {
                console.log("have params");
                const peerId = route.params.peerid;
                const fc = route.params.friendChatter;
                navigation.setOptions({
                    title: fc.name
                });
                // Refresh peer profile image
                if (fc.profile_uri ===undefined || fc.profile_uri === "") {
                    setPartnerProfile(require('../assets/defaultprofile.png'));
                } else {
                    setPartnerProfile({ uri: fc.profile_uri });
                }
                if(peerid !== peerId) {
                    console.log("changed id");
                    // New chatter
                    setPchat(true);
                    setPeerid(peerId);
                    setFriendChatter(fc);
                    setMessages([]);
                    setPartnerName(fc.name);
                    setChatStarted(false);
                    console.log(`start chat ids selfid ${_id} friendid ${fc._id} ${fc.name}`);
                    socket.emit('start-chat', { selfid: _id, friendid: fc._id });
                } else if(dbChatId !== "") {
                    console.log("re-requesting messages ", dbChatId);
                    socket.emit('request-messages', { chatId: dbChatId });
                }
            }
            if(route.params!==undefined && route.params.game!==undefined) {
                console.log("found game params", route.params);
                sendGMessage();
                route.params.game = undefined;
                route.params.score = undefined;
            } else {
                console.log("no game param");
            }
            scrollToBottom();
            return () => {
                socket.off('chatStarted');
            };
        }, [route])
    );
    useEffect(() => {
        console.log("USE EFFECT");
        socket.on('load-messages', (messageinfo) => {
            console.log("loading messages");
            setDBChatId(messageinfo._id);
            setChatStarted(true);
            setMessages(messageinfo.messages);
            // after messages are loaded in, send gmessage if we have one
            scrollToBottom();
        });
        socket.on('error', () => {
            console.error('Error connecting to socket server:', error);
        });
        socket.on('receiveMessage', (messageVal) => {
            console.log("receiving message", messageVal);
            const recMessage = { type: messageVal.type, messageContent: messageVal.messageContent, sender: "other", timestamp: messageVal.timestamp };
            setMessages(messages => [...messages, recMessage]);
        });

        if(route.params!==undefined && route.params.peerid!==undefined && route.params.peerid!=="") {
        } else {
            // start ephemeral chat
            console.log("peer not provided: starting ephemeral chat");
            socket.emit('join-matchmaking', { _id: _id, name: name });

            // Listen for 'chatStarted' event from the backend
            socket.on('chatStarted', (chatInfo) => {
                console.log("starting chat", chatInfo);
                setPartnerSocketId(chatInfo.partner.sid);
                setPartnerName(chatInfo.partner.name);
                setEPartnerUid(chatInfo.partner.uid);
                setChatStarted(true);
            });
        }

        // Clean up function
        return () => {
            // socket.disconnect();
            socket.off('load-messages');
            socket.off('error');
            socket.off('receiveMessage');
            socket.off('chatStarted');
        };
    }, []);


    const resetChat = () => {
        // Clear displayed messages & input
        setMessageContent('');
        setMessages([]);

        setPartnerName("other");
        setChatStarted(false);

        const isPChat = friendChatter !== undefined;
        setPchat(isPChat);
        if(isPChat) {
            setFriendChatter(route.params.friendChatter);
        }
    };
    const resizeImage = async (uri) => {
        const manipulatedImage = await ImageManipulator.manipulateAsync(
            uri,
            [{ resize: { width: 800 } }], // Resize to width of 800 pixels
            { compress: 0.5, format: ImageManipulator.SaveFormat.JPEG }
        );
        return manipulatedImage;
    };

    // Image upload:
    const uploadImage = async () => {
        // Ensuring that we have the necessary permissions
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
            Alert.alert('Sorry, camera roll permissions are needed to make this work!');
            return "";
        }
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
            exif: true,
        });

        if (result.cancelled) {
            console.log('User cancelled image picker');
            return "";
        }

        if (result.assets && result.assets.length > 0) {
            const originalImage = result.assets[0];
            const resizedImage = await resizeImage(originalImage.uri); // Ensure to await the resizing

            const formData = new FormData();
            formData.append('file', {
                uri: resizedImage.uri,
                name: originalImage.fileName || 'upload.jpg', // Use the original file name or a fallback
                type: originalImage.mimeType || 'image/jpeg' // Use the original MIME type or a fallback
            });

            try {
                const response = await fetch(`${BACKEND_URL}uploads`, {
                    method: 'POST',
                    body: formData,
                    headers: {
                        'Accept': 'application/json',
                        // Content-Type should not be explicitly set for FormData with fetch
                    },
                });

                if (!response.ok) {
                    console.log('HTTP Error:', response.status, await response.text());
                    throw new Error(`HTTP error status: ${response.status}`);
                }

                const jsonResponse = await response.json();
                console.log('Uploaded Image URL:', jsonResponse.url);
                return jsonResponse.url;
            } catch (error) {
                console.error('Error uploading image:', error);
            }
        } else {
            console.log('No assets to upload');
        }
        return "";
    };

    const fetchMessages = async () => {
        try {
            const response = await fetch(
                `${BACKEND_URL}messages/${userId}/${recepientId}`
            );
            const data = await response.json();

            if (response.ok) {
                setMessages(data);
                console.log("setting messages", messages);
            } else {
                console.log("error showing messags", response.status.message);
            } 
        } catch (error) {
            console.log("error fetching messages", error);
        }
    };
    const sendGMessage = () => {
        console.log("sending game message", route.params.score);
        const newMessage = { messageContent: route.params.score, timestamp: new Date().toISOString(), senderId: _id, type: "game" };
        setMessages(messages => [...messages, newMessage]);
        if (socket !== null) {
            if(pchat) {
                socket.emit('sendPMessage', { messageContent: route.params.score, chatid: dbChatId, userId: _id, type: "game" });
            } else {
                console.log("sending message to ephemeral chat");
                socket.emit('sendMessage', { messageContent: route.params.score, partnerSocketId, type: "game" });
            }
            setMessageContent('');
        } else {
            console.error("Tried to send message when socket is null");
        }
    };
    const sendMessage = () => {
        if (messageContent.trim() === '') return;
        const newMessage = { messageContent: messageContent, timestamp: new Date().toISOString(), senderId: _id, type: "text" };
        setMessages(messages => [...messages, newMessage]);
        if (socket !== null) {
            if(pchat) {
                socket.emit('sendPMessage', { messageContent, chatid: dbChatId, userId: _id, type:"text" });
            } else {
                console.log("sending message to ephemeral chat");
                socket.emit('sendMessage', { messageContent, partnerSocketId, type: "text" });
            }
            setMessageContent('');
        } else {
            console.error("Tried to send message when socket is null");
        }
    };
    const sendImgMessage = (img_url) => {
        if (img_url.trim() === '') return;
        const newMessage = { messageContent: img_url, timestamp: new Date().toISOString(), senderId: _id, type: "image" };
        setMessages(messages => [...messages, newMessage]);
        if (socket !== null) {
            if(pchat) {
                socket.emit('sendPMessage', { messageContent: img_url, chatid: dbChatId, userId: _id, type: "image" });
            } else {
                console.log("tried to send image in non persistent chat");
                // socket.emit('sendMessage', { img_url, partnerSocketId });
            }
            setMessageContent('');
        } else {
            console.error("Tried to send img message when socket is null");
        }
    };
    const pickImage = async () => {
        const imageUrl = await uploadImage();
        sendImgMessage(imageUrl);
        // // TODO
        // Alert.alert(
        //     `Select Image`, "",
        //     [
        //         {
        //             text: "Ok",
        //             onPress: () => {
        //                 uploadImage();
        //             },
        //         },
        //         {
        //             text: "Cancel", // Just dismisses dialog
        //         }
        //     ]
        // );
    };


    // const renderItem = ({ item }) => (
    //     <View style={{ alignItems: item.sender === 'self' ? 'flex-end' : 'flex-start' }}>
    //         <Text style={{ padding: 10, backgroundColor: item.sender === 'self' ? 'blue' : 'green', borderRadius: 10, margin: 5, color: 'white' }}>{item.text}</Text>
    //     </View>
    // );

    // <Image style={styles.profileImg} source={imgSource}/>
    // <Text style={{color: "white"}}>
    //     Sender: {(item.sender==="other" ? partnerName : item.sender)}
    // </Text>
    const gameOpen = () => {
        Alert.alert(
            `Choose Game`, "",
            [
                {
                    text: "Reaction Time",
                    onPress: () => {
                        console.log("starting game");
                        navigation.navigate('Reaction time', { peerid: friendChatter._id, friendChatter });
                    },
                },
                {
                    text: "Speedball",
                    onPress: () => {
                        console.log("starting game");
                        navigation.navigate('Speedball', { peerid: friendChatter._id, friendChatter });
                    },
                },
                {
                    text: "Cancel", // Just dismisses dialog
                },
            ]
        );
    };

    const frClick = () => {
        Alert.alert(
            `Send friend request?`, "",
            [
                {
                    text: "Confirm",
                    onPress: async () => {
                        sendFriendRequest();
                    },
                },
                {
                    text: "Cancel", // Just dismisses dialog
                },
            ]
        );
    };

    const scrollToBottom = () => {
        setTimeout(() => {
            if (flatListRef.current) {
                flatListRef.current.scrollToEnd({ animated: true });
            }
        }, 100);
    };
    const sendFriendRequest = async () => {
        if (ePartnerUid !== undefined && ePartnerUid !== "") {
            const senderId = _id;
            const receiverId = ePartnerUid;
            try {
                const response = await fetch(`${BACKEND_URL}friend/sendrequest`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        senderId,
                        receiverId
                    })
                });
                if (!response.ok) {
                    console.log(response.status);
                    const errorData = await response.json();
                    throw new Error(errorData.error || 'Failed to send friend request');
                } 
                const responseData = await response.json();
                console.log('Friend request sent', responseData.friendRequest);
            } catch (error) {
                console.error('Error sending friend request:', error.message);
            }
        }
    };
    const parseTimestamp = (strISOTime) => {
        if(strISOTime===undefined) {
            return "";
        }
        // NOTE: assumes EST for now; should use location for accurate time
        const date = new Date(strISOTime);
        const estTime = new Intl.DateTimeFormat('en-US', {
            timeZone: 'America/New_York',
            hour: '2-digit',
            minute: '2-digit',
            hour12: false
        }).format(date);
        return estTime;
    };
    function renderMessage(item) {
        switch (item.type) {
            case 'game':
                return (item.senderId===_id ? (
                        <View style={[styles.gmessage, styles.mymessage]}>
                            <MaterialCommunityIcons name="gamepad-variant" size={28} color="white"
                                onPress={gameOpen} style={{ marginHorizontal: 5 }}/>
                            <Text style={{ color: "white", fontSize: 16 }}>Game Score: {item.messageContent}</Text>
                            <Text style={[(item.senderId===_id ? styles.mymessage : styles.othermessage), {color: "#ccc"}]}>{parseTimestamp(item.timestamp)}</Text>
                        </View>
                    ) : (
                        <View style={[{ display: "flex", flexDirection: "row" }, styles.othermessage]}>
                            {item.senderId!==_id && 
                                <Image style={[styles.profileImg, {marginLeft: 5}]} source={partnerProfile}/>}
                            <View style={[styles.gmessage, ]}>
                                <MaterialCommunityIcons name="gamepad-variant" size={24} color="white"
                                    onPress={gameOpen} style={{ marginHorizontal: 5 }}/>
                                <Text style={{ color: "white", fontSize: 16 }}>Game Score: {item.messageContent}</Text>
                                <Text style={[(item.senderId===_id ? styles.mymessage : styles.othermessage), {color: "#ccc"}]}>{parseTimestamp(item.timestamp)}</Text>
                            </View>
                        </View>
                    ));
            case 'image':
                    return (<View style={[styles.messageImageContainer, (item.senderId===_id ? styles.mymessage : styles.othermessage)]}>
                        <Image source={{uri: item.messageContent}} style={styles.messageImg} />
                    </View>);
            case 'text':
                    return (item.senderId===_id ? (
                        <View style={[styles.message, styles.mymessage]}>
                            <Text style={{ color: "white", fontSize: 16 }}>{item.messageContent}</Text>
                            <Text style={[(item.senderId===_id ? styles.mymessage : styles.othermessage), {color: "#ccc"}]}>{parseTimestamp(item.timestamp)}</Text>
                        </View>
                    ) : (
                            <View style={[{ display: "flex", flexDirection: "row" }, styles.othermessage]}>
                                {item.senderId!==_id && 
                                    <Image style={[styles.profileImg, {marginLeft: 5}]} source={partnerProfile}/>}
                                <View style={[styles.message, ]}>
                                    <Text style={{ color: "white", fontSize: 16 }}>{item.messageContent}</Text>
                                    <Text style={[(item.senderId===_id ? styles.mymessage : styles.othermessage), {color: "#ccc"}]}>{parseTimestamp(item.timestamp)}</Text>
                                </View>
                            </View>
                        ));
            case 'systemmsg':
                return (<View><Text>{JSON.stringify(item)}</Text></View>);
            default: 
                console.log(item);
                console.error("invalid message type");
        };
    };
    return (
        <KeyboardAvoidingView keyboardVerticalOffset={headerHeight} behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            style={styles.container}>
            {chatStarted ? (
                <View style={styles.messagesContainer}>
                    <FlatList
                        ref={flatListRef}
                        data={messages}
                        renderItem={(item) => renderMessage(item.item)}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            ) : (
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text>Loading chat...</Text>
                        <ActivityIndicator size="large" />
                    </View>
                )}
            <View
                style={styles.inputContainer}
            >
                {!pchat &&
                <Pressable
                    onPress={frClick}
                    style={{ marginHorizontal: 5}}
                >
                    <MaterialCommunityIcons name="account-plus" size={24} color="gray" />
                </Pressable>}
                {pchat && <Entypo 
                    style={{ marginHorizontal: 5 }}
                    onPress={pickImage} name="camera" size={24} color="gray" />}
                <MaterialCommunityIcons name="gamepad-square" size={24} color="gray"
                    onPress={gameOpen} style={{ marginHorizontal: 5 }}/>
                <TextInput
                    value={messageContent}
                    onChangeText={setMessageContent}
                    style={styles.messageInput}
                    placeholder="Enter message..."
                />
                <Pressable
                    onPress={sendMessage}
                    style={styles.sendButton}
                >
                    <MaterialCommunityIcons name="send" size={24} color="white" />
                </Pressable>
            </View>
        </KeyboardAvoidingView>
    );
};

const styles = {
    messagesContainer: {
        flex: 1,
    },
    container: {
        flex: 1,
    },
    // Messages
    mymessage: {
        alignSelf: 'flex-end',
    },
    othermessage: {
        alignSelf: 'flex-start',
    },
    message: {
        backgroundColor: "#0077ff",
        margin: 3,
        maxWidth: "80%",
        padding: 8,
        borderRadius: 10,
    },
    gmessage: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#00dd44",
        margin: 3,
        maxWidth: "80%",
        padding: 8,
        borderRadius: 10,
    },
    // Input bar at bottom
    inputContainer: {
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderTopWidth: 1,
        borderTopColor: "#cccccc",
        marginBottom: 0,
    },
    messageInput: {
        flex: 1,
        height: 40,
        borderWidth: 1,
        borderColor: "#cccccc",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 0,
        paddingHorizontal: 10,
    },
    sendButton: {
        backgroundColor: "#0077ff",
        paddingVertical: 8,
        paddingHorizontal: 12,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 20,
    },
    profileImg: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 5,
    },
    messageImageContainer: {
        width: 200, // Adjust size according to your layout
        height: 200,
        padding: 10,
    },
    messageImg: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
    },
    timestamp: {
        color: "white",
    },
}

export default Chat2;
