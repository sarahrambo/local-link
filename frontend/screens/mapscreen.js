import React, { useState, useEffect, useContext } from 'react';
import { View, Text, Platform, Image } from 'react-native';
import { TouchableOpacity, Button } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import { Callout } from 'react-native-maps';
import { useFocusEffect } from '@react-navigation/native';

import * as Location from 'expo-location';
import { BACKEND_URL } from '../constants';
import { AuthContext } from '../AuthContext';


const MapScreen = ({ navigation }) => {
    const { userToken } = useContext(AuthContext);
    const jsonUserToken = JSON.parse(userToken);
    const [region, setRegion] = useState(null);
    const [activeUsers, setActiveUsers] = useState([]);
    const [showChatButton, setShowChatButton] = useState(false);
    const [selectedMarker, setSelectedMarker] = useState(null);



  useFocusEffect(
    React.useCallback(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        console.log("Location permission denied");
        // Set default location to Lane Stadium
        const defaultRegion = {
          latitude: 37.2197, // Default latitude
          longitude: -80.4131, // Default longitude
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        };
        setRegion(defaultRegion);
        return;
      }
  
      let location = await Location.getCurrentPositionAsync({});
      const newRegion = {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      };
      setRegion(newRegion);

            // Send the location to the server.
            fetch(`${BACKEND_URL}update-location`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                    userId: jsonUserToken._id,
                }),
            }).then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json();
                })
                .then(data => console.log(data))
                .catch((error) => {
                    console.error('Error:', error);
                });

            // Get the location data of all active users.
            fetch(`${BACKEND_URL}get-locations`)
                .then(response => {
                    console.log('Response status:', response.status);
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json();
                })
                .then(data => {
                    setActiveUsers(data);
                    console.log(data);
                })
                .catch((error) => {
                    console.error('Error:', error);
                });

        })();
    }, []));
    const beginPFChat = (user) => {
        console.log(`Chat with ${user.name}`);
        console.log(user);
        navigation.navigate('Chat', { peerid: user._id, friendChatter: user });
    };

    return (
        <View style={{ flex: 1 }}>
            {region && (
                <MapView
                    style={{ flex: 1 }}
                    region={region}
                    showsUserLocation={true}
                >
                    <Marker
                        coordinate={{
                            latitude: region.latitude,
                            longitude: region.longitude,
                        }}
                    />
                    {activeUsers.map((user, index) => (
                        user.location && (
                            <Marker
                                key={index}
                                coordinate={{
                                    latitude: user.location.latitude,
                                    longitude: user.location.longitude,
                                }}
                                title={user.name}
                            >
                                <Callout onPress={() => {
                                    console.log(`Marker ${index} pressed`);
                                    setSelectedMarker(index);
                                }}>
                                    <View>
                                        <Text style={{fontWeight: 'bold'}}>{String(user.name)}</Text>
                                            {user.profile_uri && (
                                                <Image
                                                    source={{ uri: user.profile_uri }}
                                                    style={{ width: 120, height: 120 }}
                                                />
                                            )}

                                            {user.name != jsonUserToken.name && (
                                                <Button title="Chat" onPress={() => beginPFChat(user)} />
                                            )}
                                                
                                    </View>
                                </Callout>
                            </Marker>
                        )
                    ))}
                </MapView>
            )}
        </View>
    );
};

export default MapScreen;
