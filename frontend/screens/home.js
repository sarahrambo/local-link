import React, { useContext, useState, useEffect } from 'react';
import { Text, View, StyleSheet, TextInput, FlatList, TouchableOpacity, Alert } from 'react-native';
import { AuthContext } from '../AuthContext';
import ContactListItem from '../components/Contact';
import FriendRequest from '../components/FriendRequest';
import { BACKEND_URL } from '../constants';

const sortContactsByActive = (friends) => {
  return friends.sort((a, b) => {
    if (a.active && !b.active) {
      return -1; // a before b
    } else if (!a.active && b.active) {
      return 1; // b before a
    } else {
      return 0;
    }
  });
};


const Home = () => {
  const { userToken } = useContext(AuthContext);
  const [friends, setFriends] = useState([]);
  const [friendId, setFriendId] = useState('');
  const [friendRequests, setFriendRequests] = useState([]);
  const sortedContacts = sortContactsByActive(friends);
   
  useEffect(() => {
    const fetchFriends = async () => {
      try {
        const uToken = JSON.parse(userToken);
        const response = await fetch(`${BACKEND_URL}users/${uToken._id}/friends`);
        if (!response.ok) {
          throw new Error(`Failed to fetch friends ${response.status}`);
        }
        const friendsData = await response.json();
        setFriends(friendsData);
      } catch (error) {
        console.error('Error fetching friends:', error);
      }
    };

    const fetchFriendRequests = async () => {
      try {
        // const uToken = JSON.parse(userToken);
        // const response = await fetch(`${BACKEND_URL}users/${uToken._id}/friendRequests`);
        // if (!response.ok) {
        //   throw new Error(`Failed to fetch friend requests ${response.status}`);
        // }
        // const requestsData = await response.json();
        // setFriendRequests(requestsData);

        // Mock data
        const requestData = [
          { id: 1, senderName: 'John Doe', senderId: '123'},
          { id: 2, senderName: 'Jane Smith', senderId: '456'},
        ];
        setFriendRequests(requestData);

      } catch (error) {
        console.error('Error fetching friend requests:', error);
      }
    };

    fetchFriends();
    fetchFriendRequests();

    return () => {
      // cleanup logic 
    };
  }, []);


  const handleSendFriendRequest = async () => {
    try {
      if(friendId === '') {
        Alert.alert('Error', 'Please enter a friend ID');
        return;
      }
      // const uToken = JSON.parse(userToken);
      // const response = await fetch(`${BACKEND_URL}users/${uToken._id}/sendFriendRequest`, {
      //   method: 'POST',
      //   headers: {
      //     'Content-Type': 'application/json',
      //   },
      //   body: JSON.stringify({ friendId }),
      // });
      // if (!response.ok) {
      //   throw new Error(`Failed to send friend request ${response.status}`);
      // }
      // Assuming you want to fetch friends again after sending the request
      // fetchFriends();
      // Notify user that the request was sent successfully
      Alert.alert('Friend Request Sent', 'Your friend request has been sent successfully!');
    } catch (error) {
      console.error('Error sending friend request:', error);
      // Optionally, provide feedback to the user about the error
      Alert.alert('Error', 'Failed to send friend request. Please try again later.');
    }
  };

  const handleAcceptRequest = async (requestId) => {
    try {
      // const uToken = JSON.parse(userToken);
      // const response = await fetch(`${BACKEND_URL}users/${uToken._id}/acceptFriendRequest`, {
      //   method: 'POST',
      //   headers: {
      //     'Content-Type': 'application/json',
      //   },
      //   body: JSON.stringify({ requestId }),
      // });
      // if (!response.ok) {
      //   throw new Error(`Failed to accept friend request ${response.status}`);
      // }
      // // Assuming you want to fetch friends again after accepting the request
      // fetchFriends();

      // remove the request from the list
      setFriendRequests(friendRequests.filter(request => request.id !== requestId));
    } catch (error) {
      console.error('Error accepting friend request:', error);
      // Optionally, provide feedback to the user about the error
      Alert.alert('Error', 'Failed to accept friend request. Please try again later.');
    }
  };

  const handleRejectRequest = async (requestId) => {
    try {
      // const uToken = JSON.parse(userToken);
      // const response = await fetch(`${BACKEND_URL}users/${uToken._id}/rejectFriendRequest`, {
      //   method: 'POST',
      //   headers: {
      //     'Content-Type': 'application/json',
      //   },
      //   body: JSON.stringify({ requestId }),
      // });
      // if (!response.ok) {
      //   throw new Error(`Failed to reject friend request ${response.status}`);
      // }

      // remove the request from the list
      setFriendRequests(friendRequests.filter(request => request.id !== requestId));
    } catch (error) {
      console.error('Error rejecting friend request:', error);
      // Optionally, provide feedback to the user about the error
      Alert.alert('Error', 'Failed to reject friend request. Please try again later.');
    }
  };

  return (
  <View style={styles.container}>
    {/* Friend Request Input */}
    <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Enter Friend ID"
          value={friendId}
          onChangeText={setFriendId}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={handleSendFriendRequest}>
          <Text style={styles.buttonText}>Send Request</Text>
        </TouchableOpacity>
    </View>

    {/* Friend Requests */}
    {friendRequests.map(request => (
        <FriendRequest
          key={request.id}
          request={request}
          onAccept={handleAcceptRequest}
          onReject={handleRejectRequest}
        />
    ))}

    {/* Contact List */}
    {friends.length === 0 ? 
    (<Text>No contacts yet!</Text>) : 
    (
      <FlatList style={{ width: '100%' }}
        data={sortedContacts}
        renderItem={({ item }) => <ContactListItem friend={item} />}
        keyExtractor={(item) => item._id.toString()}
      />
    )}
  </View>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    flexDirection: 'column',
  },
  inputContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  input: {
    flex: 1,
    marginRight: 10,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
  },
  button: {
    backgroundColor: '#00aaff',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
    justifyContent: 'center',
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default Home;
