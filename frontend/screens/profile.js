import React, { useContext, useState, useEffect } from 'react';
import { Text, View, Button, TextInput, Image, KeyboardAvoidingView, Platform, Alert } from 'react-native';
import CheckBox from 'expo-checkbox';
// Use auth
import { AuthContext } from '../AuthContext';
import { StyleSheet } from 'react-native';
import { BACKEND_URL } from '../constants';

const Profile = () => {
  const { userToken, authLogout } = useContext(AuthContext);
  const uToken = JSON.parse(userToken);
  const [editingProfile, setEditingProfile] = useState(false);
  const [imageUrl, setImageUrl] = useState('');
  const [imageSource, setImageSource] = useState(null);
  const [age, setAge] = useState("");
  const [gender, setGender] = useState("");
  const [occupation, setOccupation] = useState("");
  const [travel, setTravel] = useState(false)
  const [sports, setSports] = useState(false)
  const [music, setMusic] = useState(false)
  const [cooking, setCooking] = useState(false)

  const handleImageSubmit = () => {
    // TODO: Validate URL here
    // Load image to check if URL is valid
    fetchImage(imageUrl);
  };

  const fetchImage = (url) => {
    fetch(url)
      .then(response => {
        if (!response.ok) {
          throw new Error('Invalid URL');
        }
        return response.blob();
      })
      .then(blob => {
        setImageSource({ uri: url });
        try {
            saveImageToBackend(url);
        } catch(error) {
            console.error("Failed to save image URI", error);
        }
      })
      .catch(error => {
        Alert.alert('Error', 'Failed to load image. Please enter a valid URL.');
        console.error(error);
      });
  };

  const handleImageCancel = () => {
      setEditingProfile(false);
      setImageUrl('');
  };

  const startEditUri = () => {
    setEditingProfile(true);
  };

  const handleLogout = () => {
    // use auth update 
    authLogout(null);
  };
  const saveImageToBackend = (uri) => {
    fetch(`${BACKEND_URL}users/${uToken._id}/profile_uri`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            profile_uri: uri,
          }),
        })
      .then(response => {
              if (!response.ok) {
                  console.error("Failed to save image to backend", response.status, response.body);
              } else {
                  Alert.alert('Success', 'Image Saved');
                  setEditingProfile(false);
                  setImageUrl('');
              }
      })
      .catch(error => {
        console.error('Error saving image to backend:', error);
      });
  };

  const handleProfileSave = () => {
    const profileData = {
      age: age,
      gender: gender,
      occupation: occupation,
      interests: {
        travel: travel,
        sports: sports,
        music: music,
        cooking: cooking
      }
    };
    fetch(`${BACKEND_URL}users/${uToken._id}/profile_data`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify(profileData),
    }).then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(data => console.log(data))
    .catch((error) => {
        console.error('Error:', error);
    });
  };

  useEffect(() => {
    const fetchProfile = async () => {
      try {
        const uToken = JSON.parse(userToken);
        const response = await fetch(`${BACKEND_URL}users/${uToken._id}/profile_uri`);
        if (!response.ok) {
          throw new Error(`Failed to fetch users ${response.status}`);
        }
        const profile_uri_data = await response.json();
        setImageSource(profile_uri_data.profile_uri)
        // setAge(profile_uri_data.age);
        // setGender(profile_uri_data
        // setOccupation(profile_uri_data.occupation);
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };

    fetchProfile();

    return () => {
      // cleanup logic 
    };
  }, []);

  return (
    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} 
    style={styles.container}>
      {imageSource && imageSource != ""? <Image source={{uri: imageSource}} style={{ width: 200, height: 200, marginVertical: 10, borderRadius: 100 }} /> : <Image source={require('../assets/defaultprofile.png')} style={{ width: 200, height: 200, marginBottom: 10 }} />}
      {editingProfile ? (
        <>
          <TextInput
            style={{ height: 40, width: 300, borderColor: 'gray', borderWidth: 1, marginBottom: 10}}
            placeholder="Enter image URL"
            onChangeText={text => setImageUrl(text)}
            value={imageUrl}
          />
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Button
              title="Submit"
              onPress={handleImageSubmit}
            />
            <Button
              title="Cancel"
              onPress={handleImageCancel}
            />
          </View>
        </>
      ) : (
        <Button title="Change Profile URI" onPress={startEditUri}/>
      )}
        <View style={{marginVertical: 20, justifyContent: 'center', alignItems: 'center',}}>
            <TextInput
              style={{ height: 25, width: 300, marginBottom: 5}}
              placeholder="Age"
              value={'Username: ' + uToken.name}
              readOnly={true}
            />
            <TextInput
              style={{ height: 25, width: 300, marginBottom: 5}}
              placeholder="Age"
              value={'ID: ' + uToken._id}
              readOnly={true}
            />
            <TextInput
              style={{ height: 40, width: 300, borderColor: 'gray', borderWidth: 1, marginBottom: 15, padding: 5 }}
              placeholder="Age"
              onChangeText={text => setAge(text)}
              value={age}
            />
            <TextInput
              style={{ height: 40, width: 300, borderColor: 'gray', borderWidth: 1, marginBottom: 15, padding: 5 }}
              placeholder="Gender"
              onChangeText={text => setGender(text)}
              value={gender}
            />
            <TextInput
              style={{ height: 40, width: 300, borderColor: 'gray', borderWidth: 1, marginBottom: 15, padding: 5 }}
              placeholder="Occupation"
              onChangeText={text => setOccupation(text)}
              value={occupation}
            />
            <View style={styles.checkboxContainer}>
              <CheckBox
                style={styles.checkbox}
                disabled={false}
                value={travel}
                onValueChange={(newValue) => setTravel(newValue)}
              />
              <Text style={styles.label}>Travel</Text>
              <CheckBox
                style={styles.checkbox}
                disabled={false}
                value={sports}
                onValueChange={(newValue) => setSports(newValue)}
              />
              <Text style={styles.label}>Sports</Text>
              <CheckBox
                style={styles.checkbox}
                disabled={false}
                value={music}
                onValueChange={(newValue) => setMusic(newValue)}
              />
              <Text style={styles.label}>Music</Text>
              <CheckBox
                style={styles.checkbox}
                disabled={false}
                value={cooking}
                onValueChange={(newValue) => setCooking(newValue)}
              />
              <Text style={styles.label}>Cooking</Text>
            </View>
            {/* <Text>{userToken}</Text> */}
            <View style={{ flexDirection: 'row'}}> 
              <Button title="Save Profile" onPress={handleProfileSave} />
              <Button title="Logout" onPress={handleLogout} />
            </View>
        </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  checkboxContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: 'center',
  },
  label: {
    margin: 8,
  },
});

export default Profile;
