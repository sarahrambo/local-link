import React, { useState, useEffect, useRef } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Dimensions } from 'react-native';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const Game2 = ({ route, navigation }) => {
    const [buttonPosition, setButtonPosition] = useState({ x: 0, y: 0 });
    const [started, setStarted] = useState(false);
    const score = useRef(0);

    useEffect(() => {
        if (started) {
            generateNewButtonPosition();
            const timeout = setTimeout(() => {
                const finalScore = score.current;
                alert('Game Over! Your score: ' + finalScore);
                score.current = 0;
                setStarted(false);
                navigation.navigate('Chat', { 
                    peerid: route.params.peerid, 
                    friendChatter: route.params.friendChatter,
                    game: "game",
                    score: finalScore
                });

            }, 15000);
        }
    }, [started]);

    const generateNewButtonPosition = () => {
        const randomX = Math.floor(Math.random() * (SCREEN_WIDTH - 50));
        const randomY = Math.floor(Math.random() * (SCREEN_HEIGHT - 220));
        setButtonPosition({ x: randomX, y: randomY });
    };

    const handleButtonPress = () => {
        score.current += 1;
        generateNewButtonPosition();
    };

    return (
        <View style={styles.container}>
            {
                !started &&
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => setStarted(true)}>
                        <Text style={styles.buttonText}>Start</Text>
                    </TouchableOpacity>
            }
            { started &&
                <TouchableOpacity
                    activeOpacity={1}
                    style={[styles.button, { top: buttonPosition.y, left: buttonPosition.x }]}
                    onPress={handleButtonPress}>
                    <Text style={styles.buttonText}>{score.current + 1}</Text>
                </TouchableOpacity>}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    button: {
        position: 'absolute',
        width: 50, // Adjust button size as needed
        height: 50, // Adjust button size as needed
        borderRadius: 50, // Adjust button border radius as needed
        backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
    },
});

export default Game2;
