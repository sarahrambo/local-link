import React, { useState, useContext } from 'react';
import { Text, Image, View, TextInput, TouchableWithoutFeedback, 
    TouchableOpacity, Keyboard, KeyboardAvoidingView, Platform } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AppStyles from '../styles';
import { BACKEND_URL } from '../constants';
import { AuthContext } from '../AuthContext';

const Signup = () => {
    const navigation = useNavigation();
    const [createName, setCreateName] = useState('');
    const [createPassword, setCreatePassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const { authLogin } = useContext(AuthContext);

    const createAccount = async (event) => {
        if (createPassword !== confirmPassword) {
            alert('Passwords do not match');
            return;
        }
        try {
            // Check if createPassword and confirmPassword match
            const response = await fetch(`${BACKEND_URL}create-account`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json'
            },
              body: JSON.stringify({"name": createName, "password": createPassword})
            });
      
            if (response.ok) {
              const data = await response.json();
              if(data.error){
                console.log(data.error);
              }
              else {
                // Handle successful response from the backend
                let token = {
                  _id: data._id,
                  name: data.name,
                  jwt: data.jwt_token
                }
                authLogin(JSON.stringify(token));
              }
            } else if(response.status === 409){
              alert('Username already exists');
              console.log('Username already exists');
            } else {
              console.error('Error sending text.');
              // Handle error response from the backend
            }
          } catch (error) {
            console.error('Error sending text:', error);
            // Handle fetch error
          }
    }

    return(
        <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.container}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={[styles.view, styles.container]}>
                <Image source={require('../assets/locallink-logo.png')} />
                <Text style={{ fontSize: 28}}>Sign up</Text>
                <TextInput 
                    placeholder='Username' 
                    style={styles.input}
                    onChangeText={(text) => setCreateName(text)}
                />
                <TextInput 
                    placeholder='Password' 
                    style={styles.input}
                    onChangeText={(text) => setCreatePassword(text)}
                    secureTextEntry={true}
                />
                <TextInput 
                    placeholder='Confirm Password' 
                    style={styles.input}
                    onChangeText={(text) => setConfirmPassword(text)}
                    secureTextEntry={true}
                />
                <TouchableOpacity style={AppStyles.button} onPress={createAccount}>
                    <Text style={{color: '#fff', textAlign: 'center'}}>Create Account</Text>
                </TouchableOpacity>
                <Text>Already have an account? <Text style={{color: '#2196F3'}} onPress={() => navigation.navigate('Login')}>Login</Text></Text>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      );
}

const styles = {
    container: {
        flex: 1,
    },
    view: {
        justifyContent: 'center', 
        alignItems: 'center'
    },
    input: {
        borderRadius: 12,
        padding: 10, 
        width: '75%',
        marginVertical: 10,
        backgroundColor: 'white',
        height: 45
    },
}

export default Signup
