import React, { useState, useEffect, useContext } from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native';
import { PieChart } from 'react-native-chart-kit';
import { BACKEND_URL } from '../constants';
import { AuthContext } from '../AuthContext';

const DataScreen = () => {
  const { userToken, authLogout } = useContext(AuthContext);
  const uToken = JSON.parse(userToken);
  const [numberOfUsers, setNumberOfUsers] = useState(0);
  const [numberOfFriends, setNumberOfFriends] = useState(0);
  const [percentTravel, setPercentTravel] = useState(0);
  const [percentSports, setPercentSports] = useState(0);
  const [percentMusic, setPercentMusic] = useState(0);
  const [percentCooking, setPercentCooking] = useState(0);
  const [pieChartData, setPieChartData] = useState([]);

  // Function to fetch the number of users from the database
  const fetchNumberOfUsers = async () => {
    try {
      const response = await fetch(`${BACKEND_URL}all-users`); // API endpoint
      // console.log(`Response status: ${response.status}`);
      // console.log(`Response status text: ${response.statusText}`);
      if (!response.ok) {
        console.error(`API error: ${response.status} - ${response.statusText}`);
        return 0; // Return 0 in case of an error
      }
      const contentType = response.headers.get('Content-Type');
      // console.log(`Content-Type: ${contentType}`);
      if (!contentType || !contentType.includes('application/json')) {
        console.error(`Unexpected Content-Type: ${contentType}`);
        return 0; // Return 0 if the content type is not JSON
      }
      const data = await response.json();
      const numberOfUsers = data.filter(user => user.active).length;
      return numberOfUsers;
    } catch (error) {
      console.error('Failed to fetch the number of users:', error);
      return 0; // Return 0 if there is an error
    }
  };

  const fetchFriends = async () => {
    try {
      const response = await fetch(`${BACKEND_URL}users/${uToken._id}/friends`);
      if (!response.ok) {
        throw new Error(`Failed to fetch friends ${response.status}`);
      }
      const friendsData = await response.json();
      return friendsData
    } catch (error) {
      console.error('Error fetching friends:', error);
    }
  };

  // Fetch appropriate data to display on page
  useEffect(() => {
      const fetchData = async () => {
        const numUsers = await fetchNumberOfUsers();
        const friends = await fetchFriends();
        setNumberOfUsers(numUsers);
        setNumberOfFriends(friends.length);
        
        let enjoyTravel = 0
        let enjoySports = 0
        let enjoyMusic = 0
        let enjoyCooking = 0
        for(let i = 0; i < friends.length; i++) {
          if(friends[i].profile_data) {
            if(friends[i].profile_data.interests.travel) {
              enjoyTravel++
            }
            if(friends[i].profile_data.interests.sports) {
              enjoySports++
            }
            if(friends[i].profile_data.interests.music) {
              enjoyMusic++
            }
            if(friends[i].profile_data.interests.cooking) {
              enjoyCooking++
            }
          }
        }
        setPercentTravel((enjoyTravel / friends.length) * 100)
        setPercentSports((enjoySports / friends.length) * 100)
        setPercentMusic((enjoyMusic / friends.length) * 100)
        setPercentCooking((enjoyCooking / friends.length) * 100)
        
        let data = [
          { name: '< 10', population: 0, color: '#6074e7', legendFontColor: '#7F7F7F', legendFontSize: 10 },
          { name: '11-20', population: 0, color: '#d0b8e9', legendFontColor: '#7F7F7F', legendFontSize: 10 },
          { name: '21-30', population: 0, color: '#ea22cb', legendFontColor: '#7F7F7F', legendFontSize: 10 },
          { name: '31-40', population: 0, color: '#8c63e2', legendFontColor: '#7F7F7F', legendFontSize: 10 },
          { name: '41-50', population: 0, color: '#e26a00', legendFontColor: '#7F7F7F', legendFontSize: 10 },
          { name: '51-60', population: 0, color: '#76eab6', legendFontColor: '#7F7F7F', legendFontSize: 10 },
          { name: '61+', population: 0, color: '#419993', legendFontColor: '#7F7F7F', legendFontSize: 10 }
        ];
        // Loop through profile data to count ages
        friends.forEach(friend => {
          if (friend.profile_data && friend.profile_data.age) {
            const age = parseInt(friend.profile_data.age);
            if (age < 10) {
              data[0].population++;
            } else if (age >= 11 && age <= 20) {
              data[1].population++;
            } else if (age >= 21 && age <= 30) {
              data[2].population++;
            } else if (age >= 31 && age <= 40) {
              data[3].population++;
            } else if (age >= 41 && age <= 50) {
              data[4].population++;
            } else if (age >= 51 && age <= 60) {
              data[5].population++;
            } else {
              data[6].population++;
            }
          }
        });
        setPieChartData(data);
      };

      fetchData();
  }, []);

  return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <View style={{flexDirection: 'row'}}>
            <View style={[styles.tile, styles.smallTile]}>
              <Text style={{fontSize: 35, fontWeight: 'bold'}}>{numberOfFriends}</Text>
              <Text style={{fontSize: 20}}>Friends</Text>
            </View>
            <View style={[styles.tile, styles.smallTile]}>
              <Text style={{fontSize: 35, fontWeight: 'bold'}}>{numberOfUsers}</Text>
              <Text style={{fontSize: 20}}>Total Users</Text>
            </View>
          </View>
          <Text style={{fontSize: 25, marginLeft: 20, marginTop: 15, fontWeight: 'bold'}}>Age Distribution</Text>
          <View style={[styles.tile, styles.largeTile]}>
            <PieChart
              data={pieChartData}
              width={Dimensions.get('window').width}
              height={Dimensions.get('window').height * 0.3}
              chartConfig={{
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              }}
              accessor={"population"}
              backgroundColor={"transparent"}
              paddingLeft={"15"}
            />
          </View>
          <Text style={{fontSize: 25, marginLeft: 20, marginTop: 15, fontWeight: 'bold'}}>Friend Interests</Text>
          <View style={{flexDirection: 'row'}}>
            <View style={[styles.tile, styles.smallTile]}>
              <Text style={{fontSize: 20}}>
                <Text style={{fontWeight: 'bold', fontSize: 25}}>{percentTravel.toFixed(2)}%</Text> {"\n"}enjoy traveling
              </Text>
            </View>
            <View style={[styles.tile, styles.smallTile]}>
              <Text style={{fontSize: 20, flexDirection: 'column'}}>
                <Text style={{fontWeight: 'bold', fontSize: 25}}>{percentSports.toFixed(2)}%</Text> {"\n"}enjoy sports
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={[styles.tile, styles.smallTile]}>
                <Text style={{fontSize: 20}}>
                  <Text style={{fontWeight: 'bold', fontSize: 25}}>{percentMusic.toFixed(2)}%</Text> {"\n"}enjoy music
                </Text>
              </View>
              <View style={[styles.tile, styles.smallTile]}>
                <Text style={{fontSize: 20}}>
                  <Text style={{fontWeight: 'bold', fontSize: 25}}>{percentCooking.toFixed(2)}%</Text> {"\n"}enjoy cooking
                </Text>
              </View>
            </View>
        </ScrollView>
      </View>
  );
};

const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
  },
  tile: {
    borderRadius: 10,
    backgroundColor: 'white',
    shadowColor: '#171717',
    shadowOffset: {width: -10, height: 10},
    shadowOpacity: 0.05,
    shadowRadius: 5,
    margin: 10,
  },
  wideTile: {
    height: 110,
    width: Dimensions.get('window').width * 0.9,
    alignItems: 'center',
    justifyContent: 'center'
  },
  smallTile: {
    height: 110,
    width: Dimensions.get('window').width * 0.43,
    alignItems: 'center',
    justifyContent: 'center'
  },
  largeTile: {
    height: Dimensions.get('window').width * 0.65,
    width: Dimensions.get('window').width * 0.9,
  }
});

export default DataScreen;
