import React, { useState, useEffect, useContext } from 'react';
import { Text, Image, View, TextInput, TouchableWithoutFeedback, 
  TouchableOpacity, Keyboard, KeyboardAvoidingView, Platform, PermissionsAndroid } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AppStyles from '../styles';
import { BACKEND_URL } from '../constants';
import { AuthContext } from '../AuthContext';
import * as Location from 'expo-location';

const Login = () => {
    const navigation = useNavigation();
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    
    const { authLogin } = useContext(AuthContext);

    const handleLogin = async (event) => {
      // check database for user
      try {
        // TODO: Check if createPassword and confirmPassword match
        const response = await fetch(`${BACKEND_URL}login`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
        },
          body: JSON.stringify({name: name, password: password})
        });
  
        if (response.ok) {
          const data = await response.json();
          if(data.error){
            console.log(data.error);
          }
          else {
            // Handle successful response from the backend
            let token = {
              _id: data._id,
              name: data.name,
              jwt: data.jwt_token
            }
            authLogin(JSON.stringify(token)); // TODO: update this
            // navigation.navigate('Tabs', {_id: data._id, name: data.name});
            // navigation.navigate('Home');
          }

          try { // Get user location
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
              console.log("Location permission denied");
              // Set default location to Lane Stadium
              const defaultRegion = {
                latitude: 37.2197, // Default latitude
                longitude: -80.4131, // Default longitude
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              };
              setRegion(defaultRegion);
              return;
            }
          } catch (err) {
            console.warn(err);
          }

        } else if(response.status === 401) {
          alert('Invalid login credentials');
          //console.error('Invalid login credentials');
        } else if(response.status === 404) {
          alert('User not found');
          //console.error('User not found');
        } else {
          console.error('Error sending info to backend', response.status);
          // Handle error response from the backend
        }
      } catch (error) {
        console.error('Error logging in:', error);
        // Handle fetch error
      }
    }

    return(
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={[styles.view, styles.container]}>
              <Image source={require('../assets/locallink-logo.png')} />
              <Text style={{ fontSize: 28}}>Login</Text>
              <TextInput 
                  placeholder='Username/Email' 
                  style={styles.input}
                  onChangeText={(text) => setName(text)}
              />
              <TextInput 
                  placeholder='Password' 
                  style={styles.input}
                  onChangeText={(text) => setPassword(text)}
                  secureTextEntry={true}
              />
              <TouchableOpacity style={AppStyles.button} onPress={handleLogin}>
                <Text style={{color: '#fff', textAlign: 'center'}}>Login</Text>
              </TouchableOpacity>
              <Text style={{color: '#2196F3'}}>Forgot Password</Text>
          </View>
        </TouchableWithoutFeedback>
        <View style={[styles.view, {marginBottom: 40}]}>
          <Text style={{maxWidth: "80%", textAlign: "center"}}>Don't have an account? Sign up <Text style={{color: '#2196F3'}} onPress={() => navigation.navigate('Signup')}>here </Text>
          </Text>
        </View>
      </KeyboardAvoidingView>
    );
}

const styles = {
    container: {
      flex: 1,
    },
    view: {
      justifyContent: 'center', 
      alignItems: 'center'
    },
    input: {
      borderRadius: 12,
      padding: 10, 
      width: '75%',
      marginVertical: 10,
      backgroundColor: 'white',
      height: 45
    },
  }

export default Login;
