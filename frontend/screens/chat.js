import React, { useState, useEffect, useContext } from 'react';
import { Text, View, Button, TextInput, Platform, FlatList, TouchableWithoutFeedback, Keyboard, KeyboardAvoidingView } from 'react-native';
import io from 'socket.io-client';
import { BACKEND_URL } from '../constants';
import { AuthContext } from '../AuthContext';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ActivityIndicator } from 'react-native';
import { useSocket } from '../SocketContext';

const Chat = () => {
  const { userToken } = useContext(AuthContext);
  const jsonUserToken = JSON.parse(userToken);
  const _id = jsonUserToken._id;
  const name = jsonUserToken.name;

  const socket = useSocket();
  const [messages, setMessages] = useState([]);
  const [partnerName, setPartnerName] = useState("other");
  const [messageContent, setMessageContent] = useState('');
  const [partnerSocketId, setPartnerSocketId] = useState(null);
  const [chatStarted, setChatStarted] = useState(false);

  useEffect(() => {
    socket.on('error', () => {
        console.error('Error connecting to socket server:', error);
    });
    socket.emit('join-matchmaking', { _id: _id, name: name });
    // Listen for 'chatStarted' event from the backend
    socket.on('chatStarted', (chatInfo) => {
      console.log("starting chat", chatInfo);
        setPartnerSocketId(chatInfo.partner.sid);
        setPartnerName(chatInfo.partner.name);
        setChatStarted(true);
    });
    socket.on('receiveMessage', (messageVal) => {
        const recMessage = { text: messageVal.messageContent, sender: "other" };
        setMessages(messages => [...messages, recMessage]);
    });

    // Clean up function
    return () => {
      // socket.disconnect();
    };
  }, []);

      const sendMessage = () => {
        if (messageContent.trim() === '') return;
        const newMessage = { text: messageContent, sender: 'self' };
        setMessages(messages => [...messages, newMessage]);
        if (socket !== null) {
            socket.emit('sendMessage', { messageContent, partnerSocketId });
            setMessageContent('');
        } else {
            console.error("Tried to send message when socket is null");
        }
      };


  const renderItem = ({ item }) => (
    <View style={{ alignItems: item.sender === 'self' ? 'flex-end' : 'flex-start' }}>
      <Text style={{ padding: 10, backgroundColor: item.sender === 'self' ? 'blue' : 'green', borderRadius: 10, margin: 5, color: 'white' }}>{item.text}</Text>
    </View>
  );

  return (
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
    {chatStarted ? (
    <View style={{ flex: 1, justifyContent: 'center', padding: 20 }}>
        <FlatList
          data={messages}
          renderItem={({ item }) => (
            <View style={[styles.message, (item.sender==="self" ? styles.mymessage : styles.othermessage)]}>
            <Text style={{color: "white"}}>
              Sender: {(item.sender==="other" ? partnerName : item.sender)}
            </Text>
            <Text>{item.text}</Text>
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
        />
            <TextInput
                style={{ height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 10, paddingHorizontal: 10 }}
                onChangeText={setMessageContent}
                value={messageContent}
            />
            <Button title="Send" onPress={sendMessage} />
    </View >) : (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Joining chat...</Text>
      <ActivityIndicator size="large" />
    </View>
    )}
  </KeyboardAvoidingView>
  );
};

const styles = {
    mymessage: {
        alignSelf: 'flex-end',
    },
    othermessage: {
        alignSelf: 'flex-start',
    },
    message: {
        backgroundColor: "#00bbff",
        margin: 3,
        maxWidth: "80%",
        padding: 8,
        borderRadius: 10,
    },
  view: {
    marginTop: 100,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  input: {
    borderWidth: 1,
    padding: 10,
    width: 200,
    marginVertical: 10
  },
    container: {
    flex: 1,
    },
}

export default Chat;
