import React, { useState, useEffect } from 'react';
import { View, Text, Alert, TouchableOpacity, StyleSheet } from 'react-native';

const Game = ({ route, navigation }) => {
    const [started, setStarted] = useState(false);
    const [isGreen, setIsGreen] = useState(false);
    const [startTime, setStartTime] = useState(null);
    const [reactionTime, setReactionTime] = useState(null);
    const [plays, setPlays] = useState(0);
    const [scores, setScores] = useState([]);
    let timeout;

    useEffect(() => {
        if(started) {
            // random time between 1 and 5 seconds
            const randTime = 1000 + Math.random() * 4000;
            // console.log(randTime);
            timeout = setTimeout(() => {
                setIsGreen(true);
                setStartTime(new Date());
            }, randTime);
        }
    }, [started]);

    useEffect(() => {
        const maxPlays = 5;
        if(plays >= maxPlays && scores.length >= maxPlays){
            let sum = 0;
            for(let i = 0; i < scores.length; i++){
                sum += scores[i];
            }
            let avg = Math.round(sum / scores.length);
            setPlays(0);
            setScores([]);
            Alert.alert("GAME OVER", `Scores: ${scores.join(', ')}\n Average reaction time: ${avg}ms`);
            navigation.navigate('Chat', { 
                peerid: route.params.peerid, 
                friendChatter: route.params.friendChatter,
                game: "game",
                score: avg 
            });

        }
    }, [plays, scores]);

    const handleStart = () => {
        setReactionTime(null);
        setStarted(true);
    }

    const handlePress = () => {
        // valid press
        if(isGreen){
            const reactionTime = new Date() - startTime;
            setReactionTime(reactionTime);
            setScores([...scores, reactionTime]);
            setPlays(plays + 1);
        }
        // early press
        else{
            clearTimeout(timeout);
            setReactionTime(-1);
        }
        setStarted(false);
        setIsGreen(false);
        // setStartTime(null);
    };

    return (
        <View style={styles.container}>
            {!started && 
                <TouchableOpacity
                    style={[styles.button, { backgroundColor: 'blue' }]}
                    onPress={handleStart}>
                    <Text style={styles.buttonText}>Start</Text>
                </TouchableOpacity>}
            {started && 
                <TouchableOpacity
                    style={[styles.button, { backgroundColor: isGreen ? 'green' : 'red' }]}
                    onPress={handlePress}
                    activeOpacity={isGreen ? 1 : 0.5}>
                    <Text style={styles.buttonText}>{isGreen ? 'Tap Now!' : 'Wait for Green'}</Text>
                </TouchableOpacity>}
            {reactionTime && <Text style={styles.reactionTimeText}>{reactionTime >= 0 ? `Your reaction time: ${reactionTime} milliseconds` : "You pressed too early!"}</Text>}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    button: {
        width: 200,
        height: 200,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
    },
    buttonText: {
        color: 'white',
        fontSize: 20,
    },
    reactionTimeText: {
        marginTop: 20,
        color: 'white',
        fontSize: 16,
    },
});

export default Game;
