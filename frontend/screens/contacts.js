import React, { useContext, useState, useEffect } from 'react';
import { TouchableOpacity, Text, View, Pressable, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import { AuthContext } from '../AuthContext';
import ContactListItem from '../components/Contact';
import UserListItem from '../components/UserListItem';
import { BACKEND_URL } from '../constants';
import FriendReq from "../components/FriendReq";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useFocusEffect } from '@react-navigation/native';

import { useSocket } from '../SocketContext';
import * as Location from 'expo-location';

const styles = StyleSheet.create({
    pageContainer: {
        width: "100%",
        flex: 1,
    },
    sectionHeader: {
        width: "100%",
        paddingVertical: 10,
        paddingHorizontal: 20,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    horDivider: {
        borderBottomColor: "black",
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    // Tab buttons
    tabContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        padding: 0,
        backgroundColor: '#eee',
    },
    tabButton: {
        paddingVertical: 14,
        width: "50%",
    },
    tabText: {
        fontSize: 16,
        textAlign: "center",
    },
    // Tab content
    currTab: {
        borderBottomColor: "#4499ff",
        borderBottomWidth: 3,
    },
    content: {
    },
});
const Contacts = ({ navigation }) => {
    const { userToken, authLogout } = useContext(AuthContext);
    const uToken = JSON.parse(userToken);
    const [currTab, setCurrTab] = useState(0);
    // Tab 0: Chats
    const [nearbyUsers, setNearbyUsers] = useState([]);
    const [showUsers, setShowUsers] = useState(true);
    const [friendsList, setFriendsList] = useState([]);
    const [showFs, setShowFs] = useState(true);
    // Tab 1: Friend requests
    const [friendReqs, setFriendReqs] = useState([]);
    const [showFRs, setShowFRs] = useState(true);
    const [sentFriendReqs, setSentFriendReqs] = useState([]);
    const [showSFRs, setShowSFRs] = useState(true);

    const [fetchingLocation, setFetchingLocation] = useState(false);

    const socket = useSocket();
    // Rerun on every return to screen
    useFocusEffect(
        React.useCallback(() => {
            fetchFriendReqs();
            fetchFriends();
            fetchSentReqs();
        }, [])
    );

    useEffect(() => {
        updateLocation();  // also fetches nearby users
        fetchFriendReqs();
        fetchSentReqs();

        // stuff to do when user opens app
        socket.emit('active-users', { uid: uToken._id, name: uToken.name });
    }, []);

    const updateLocation = async () => {
        setFetchingLocation(true);
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== 'granted') {
            console.log("Location permission denied");
            return;
        }
    
        let location = await Location.getCurrentPositionAsync({});
    
        // Send the location to the server.
        fetch(`${BACKEND_URL}update-location`, {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
            userId: uToken._id,
            }),
        }).then(response => {
            if (!response.ok) {
            throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => console.log(data))
        .catch((error) => {
            console.error('Error:', error);
        });
        fetchNearbyUsers(location.coords.latitude, location.coords.longitude);
    };

    // Data Fetching Methods
    const fetchNearbyUsers = async (lat, lon) => {
      try {
        // TODO: when location-based matchings are implemented in backend, change usage here
        const response = await fetch(`${BACKEND_URL}get-nearby-users`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userId: uToken._id,
                radius: 5,
                latitude: lat,
                longitude: lon,
                requireActive: true,
            }),
        });
        if (!response.ok) {
          throw new Error(`Failed to fetch active nearby users ${response.status}`);
        }
        const users = await response.json();
        setNearbyUsers(users);
        setFetchingLocation(false);

      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };
    const fetchFriends = async () => {
      try {
        const response = await fetch(`${BACKEND_URL}users/${uToken._id}/friends`);
        if (!response.ok) {
          throw new Error(`Failed to fetch friends ${response.status}`);
        }
        const friendsData = await response.json();
        setFriendsList(friendsData);
      } catch (error) {
        console.error('Error fetching friends:', error);
      }
    };
    const fetchSentReqs = async () => {
        try {
            const response = await fetch(`${BACKEND_URL}friend/requests/outgoing/${uToken._id}`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
            });
            if (response.status === 200) {
                const data = await response.json();
                const friendRequestsData = data.friendRequests.map((friendRequestItem) => ({
                    reqId: friendRequestItem.reqId,
                    receiver: {
                        _id: friendRequestItem.receiver._id,
                        name: friendRequestItem.receiver.name,
                        profile_uri: friendRequestItem.receiver.profile_uri,
                    }
                }));

                setSentFriendReqs(friendRequestsData);
            } else {
                console.log("fetching sfrs failed", response.status);
                // setSentFriendReqs([
                //     { reqId: "", receiver: { _id: "0abc", name: "Test user", profile_uri: "", }},
                //     { reqId: "", receiver: { _id: "1abc", name: "Test user1", profile_uri: "", }},
                //     { reqId: "", receiver: { _id: "2abc", name: "Test user2", profile_uri: "", }},
                // ]);
            }
        } catch (err) {
            console.log("error message", err);
            // setSentFriendReqs([
            //         { reqId: "", receiver: { _id: "0abc", name: "Test user", profile_uri: "", }},
            //         { reqId: "", receiver: { _id: "1abc", name: "Test user1", profile_uri: "", }},
            //         { reqId: "", receiver: { _id: "2abc", name: "Test user2", profile_uri: "", }},
            // ]);
        }
    };
    // TODO: in backend, deal with cross-pending frs (user0 requests user1, user1 requests user0)
    //      Should just direct user to accept incoming request, or at least delete duplicate upon an acceptance
    const fetchFriendReqs = async () => {
        try {
            const response = await fetch(`${BACKEND_URL}friend/requests/incoming/${uToken._id}`,
                {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
            if (response.status === 200) {
                const data = await response.json();
                const friendRequestsData = data.friendRequests.map((friendRequestItem) => ({
                    reqId: friendRequestItem.reqId,
                    sender: {
                        _id: friendRequestItem.sender._id,
                        name: friendRequestItem.sender.name,
                        profile_uri: friendRequestItem.sender.profile_uri,
                    }
                }));

                setFriendReqs(friendRequestsData);
            } else {
                console.log("fetching frs failed", response.status);
                // setFriendReqs([
                //     { reqId: "", sender: { _id: "0abc", name: "Test user", profile_uri: "", }},
                //     { reqId: "", sender: { _id: "1abc", name: "Test user1", profile_uri: "", }},
                //     { reqId: "", sender: { _id: "2abc", name: "Test user2", profile_uri: "", }},
                // ]);
            }
        } catch (err) {
            console.log("error message", err);
            // console.log("setting dummy data");
            // setFriendReqs([
            //     { reqId: "", sender: { _id: "0abc", name: "Test user", profile_uri: "", }},
            //     { reqId: "", sender: { _id: "1abc", name: "Test user1", profile_uri: "", }},
            //     { reqId: "", sender: { _id: "2abc", name: "Test user2", profile_uri: "", }},
            // ]);
        }
    };
    
    const refreshFRData = (action) => {
        fetchFriendReqs();
        fetchFriends();
    };
    const refreshSFRData = (action) => {
        fetchSentReqs();
        fetchFriends();
    };
    const sentFriendReq = () => {
        fetchSentReqs();
    };
    const handleTabPress = (index) => {
        setCurrTab(index);
    };
    // TODO: fix full scroll functionality when flatlist is very long
    return (
        <View style={styles.pageContainer}>
            <View style={styles.tabContainer}>
                <TouchableOpacity
                    style={[styles.tabButton, currTab === 0 && styles.currTab]}
                    onPress={() => handleTabPress(0)}
                >
                    <Text style={styles.tabText}>Chats</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.tabButton, currTab === 1 && styles.currTab]}
                    onPress={() => handleTabPress(1)}
                >
                    <Text style={styles.tabText}>Friend Requests</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.horDivider}/>
            {currTab === 1 ? (
                <View style={styles.content}>
                    <View style={{ paddingVertical: 10 }}>
                        <Pressable 
                            onPress={()=>setShowFRs(!showFRs)}
                            style={styles.sectionHeader}>
                            <Text style={{ fontSize: 18}}>Friend Requests</Text>
                            <Icon
                                name="chevron-down"
                                size={34}
                                color="black"
                                style={{ transform: [{ rotate: showFRs ? '0deg' : '-90deg' }] }}
                            />
                        </Pressable>
                        {showFRs && <View style={{ height:  (friendReqs.length==0 ? 60 : (friendReqs.length > 5 ? 380 : 72*friendReqs.length)) }}>
                            <FlatList style={{ width: '100%' }}
                                data={friendReqs}
                                renderItem={({item}) => <FriendReq uid={uToken._id} reqId={item.reqId} prospect={item.sender} outgoing={false} responseCallback={refreshFRData}/>}
                            />
                            {friendReqs.length == 0 && 
                                <Text style={{ paddingVertical: 10, fontSize: 16, textAlign: "center"}}>
                                    No incoming friend requests!
                                </Text>}
                        </View>}
                    </View>
                    <View style={styles.horDivider}/>
                    <View style={{ paddingVertical: 10 }}>
                        <Pressable 
                            onPress={()=>setShowSFRs(!showSFRs)}
                            style={styles.sectionHeader}>
                            <Text style={{ fontSize: 18}}>Sent Requests</Text>
                            <Icon
                                name="chevron-down"
                                size={34}
                                color="black"
                                style={{ transform: [{ rotate: showSFRs ? '0deg' : '-90deg' }] }}
                            />
                        </Pressable>
                        {showSFRs && <View style={{ height: (sentFriendReqs.length==0 ? 60 : (sentFriendReqs.length > 5 ? 380 : 72*sentFriendReqs.length)) }}>
                            <FlatList style={{ width: '100%' }}
                                data={sentFriendReqs}
                                renderItem={({item}) => <FriendReq uid={uToken._id} reqId={item?.reqId} prospect={item?.receiver} outgoing={true} responseCallback={refreshSFRData}/>}
                            />
                            {sentFriendReqs.length == 0 && <Text style={{ paddingVertical: 10, fontSize: 16, textAlign: "center"}}>
                                No sent friend requests!
                            </Text>}
                        </View>}
                    </View>
                </View>
            ) : (
                <View style={styles.content}>
                    <View style={{ paddingVertical: 10 }}>
                        <Pressable 
                            onPress={()=>setShowUsers(!showUsers)}
                            style={styles.sectionHeader}>
                            <Text style={{ fontSize: 18}}>Nearby Users</Text>
                            <Icon
                                name="chevron-down"
                                size={34}
                                color="black"
                                style={{ transform: [{ rotate: showUsers ? '0deg' : '-90deg' }] }}
                            />
                        </Pressable>
                        {showUsers && <View style={{ height: (nearbyUsers.length==0 ? 60: (nearbyUsers.length > 5 ? 380 : 72*nearbyUsers.length)) }}>
                            {fetchingLocation ? 
                                <ActivityIndicator size="large" color="#0000ff" />
                                :
                                (nearbyUsers.length == 0 ? 
                                    <Text style={{ paddingVertical: 10, fontSize: 16, textAlign: "center"}}>
                                        No active users!
                                    </Text>
                                    :
                                    <FlatList style={{ width: '100%' }}
                                    data={nearbyUsers}
                                    renderItem={({item}) => <UserListItem selfuid={uToken._id} user={item} frCallback={sentFriendReq} navigation={navigation}/>}
                                    />
                                )
                            }
                        </View>}
                    </View>
                    <View style={styles.horDivider}/>
                    <View style={{ paddingVertical: 10 }}>
                        <Pressable 
                            onPress={()=>{setShowFs(!showFs)}}
                            style={styles.sectionHeader}>
                            <Text style={{ fontSize: 18}}>Friends</Text>
                            <Icon
                                name="chevron-down"
                                size={34}
                                color="black"
                                style={{ transform: [{ rotate: showFs ? '0deg' : '-90deg' }] }}
                            />
                        </Pressable>
                        {showFs && <View style={{ height: (friendsList.length==0 ? 60 : (friendsList.length > 5 ? 380 : 72*friendsList.length)) }}>
                            <FlatList style={{ width: '100%' }}
                                data={friendsList}
                                renderItem={({item}) => <ContactListItem friend={item} navigation={navigation} />}
                            />
                            {friendsList.length == 0 && <Text style={{ paddingVertical: 10, fontSize: 16, textAlign: "center"}}>
                                No contacts yet!
                            </Text>}
                        </View>}
                    </View>
                </View>
            )}
        </View>
    );
};

export default Contacts;
