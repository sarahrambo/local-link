import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from './screens/login';
import Chat from './screens/chat';
import Signup from './screens/signup';
import Map from './screens/map';

const Stack = createNativeStackNavigator();

export default function HomeStack() {
    return <NavigationContainer>
        <Stack.Navigator screenOptions={{ 
                contentStyle: {backgroundColor: '#E8EEF1'}, 
                headerShown: false,
                gestureEnabled: false,
                animation: 'none'
            }}>
            <Stack.Screen name="Login" component={Login}/>
            <Stack.Screen name="Signup" component={Signup}/>
            <Stack.Screen name="Chat" component={Chat}/>
            <Stack.Screen name="Map" component={Map}/>

        </Stack.Navigator>
    </NavigationContainer>
}