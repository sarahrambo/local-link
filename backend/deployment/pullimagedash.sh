#!/usr/bin/bash

kubectl scale --replicas=0 deployment/react-dashboard -n locallink-ns
sleep 2
kubectl get deployments -n locallink-ns
kubectl scale --replicas=1 deployment/react-dashboard -n locallink-ns
# sleep 5
# kubectl get deployments -n locallink-ns

