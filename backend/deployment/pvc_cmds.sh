#!/usr/bin/bash

project_namespace=locallink-ns

kubectl get pvc -n $project_namespace

# Get status of project pod(s)
kubectl get pod -n $project_namespace


# # Get specific pod status:
# kubectl get pod image-storage-pod -n locallink-ns
# # Shell to container running in pod:
# kubectl exec -it image-storage-pod -n locallink-ns -- /bin/bash
