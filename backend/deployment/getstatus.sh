#!/usr/bin/bash

kubectl get deployments -n locallink-ns
kubectl get services -n locallink-ns
kubectl rollout status deployment/node-backend -n locallink-ns
kubectl rollout status deployment/react-dashboard -n locallink-ns



# kubectl get services -n locallink-ns -o yaml
