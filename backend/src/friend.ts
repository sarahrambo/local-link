import express, { Request, Response } from 'express';
import { ObjectId, Db } from 'mongodb';

const friendRouter = (db: Db) => {
    const router = express.Router();
    // TODO: handle redundant/repeated friend requests
    // TODO: handle attempted self-friending requests
    // Send a friend request
    router.post('/sendrequest', async (req: Request, res: Response) => {
        const { senderId, receiverId } = req.body;
        try {
            // TODO: check if friend request exists with this sender and receiver
            // If exact match -> don't do anything
            // If a friend request was sent by the other person, just accept the request
            const friendRequest = {
                sender: new ObjectId(senderId),
                receiver: new ObjectId(receiverId),
                status: 'pending'
            };
            const result = await db
                .collection('friendrequests')
                .insertOne(friendRequest);
            console.log("friend request endpoint", result);
            res.status(201).json({ message: 'Friend request sent successfully', friendRequest: result });
        } catch (err) {
            console.error('Error sending friend request:', err);
            res.status(500).json({ error: 'Failed to send friend request' });
        }
    });

    // Get all friend requests for a user (friend request objects as represented in collection)
    router.get('/requests/all/:userId', async (req: Request, res: Response) => {
        const userId = req.params.userId;
        try {
            // Find all friend requests where the user is either the sender or receiver
            const friendRequests = await db.collection('friendrequests').find({
                $or: [{ sender: new ObjectId(userId) }, { receiver: new ObjectId(userId) }]
            }).toArray();

            res.json({ friendRequests });
        } catch (err) {
            console.error('Error fetching friend requests:', err);
            res.status(500).json({ error: 'Failed to fetch friend requests' });
        }
    });

    // Pending friend requests for user
    router.get('/requests/incoming/:userId', async (req: Request, res: Response) => {
        const userId = req.params.userId;
        try {
            // Aggregate to join friend requests with sender user information
            const friendRequests = await db.collection('friendrequests').aggregate([
                {
                    $match: { receiver: new ObjectId(userId) }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'sender',
                        foreignField: '_id',
                        as: 'senderInfo'
                    }
                },
                {
                    $unwind: '$senderInfo'
                },
                {
                    $project: {
                        reqId: '$_id',
                        sender: {
                            _id: '$senderInfo._id',
                            name: '$senderInfo.name',
                            profile_uri: '$senderInfo.profile_uri'
                        }
                    }
                }
            ]).toArray();
            res.json({ friendRequests });
        } catch (err) {
            console.error('Error fetching friend requests:', err);
            res.status(500).json({ error: 'Failed to fetch friend requests' });
        }
    });

    // Pending friend requests sent by user
    router.get('/requests/outgoing/:userId', async (req: Request, res: Response) => {
        const userId = req.params.userId;
        try {
            // Aggregate to join friend requests with sender user information
            const friendRequests = await db.collection('friendrequests').aggregate([
                {
                    $match: { sender: new ObjectId(userId) }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'receiver',
                        foreignField: '_id',
                        as: 'recInfo'
                    }
                },
                {
                    $unwind: '$recInfo'
                },
                {
                    $project: {
                        reqId: '$_id',
                        receiver: {
                            _id: '$recInfo._id',
                            name: '$recInfo.name',
                            profile_uri: '$recInfo.profile_uri'
                        }
                    }
                }
            ]).toArray();
            res.json({ friendRequests });
        } catch (err) {
            console.error('Error fetching outgoing friend requests:', err);
            res.status(500).json({ error: 'Failed to fetch outgoing friend requests' });
        }
    });

    // Delete friend request
    router.delete('/delete/:requestId', async (req: Request, res: Response) => {
        const requestId = req.params.requestId;
        try {
            // Delete the friend request
            await db
            .collection('friendrequests')
            .deleteOne({ _id: new ObjectId(requestId) });

            res.status(200).json({ message: 'Friend request deleted successfully' });
        } catch (err) {
            console.error('Error deleting friend request:', err);
            res.status(500).json({ error: 'Failed to delete friend request' });
        }
    });

    // Accept friend request
    router.put('/accept/:requestId', async (req: Request, res: Response) => {
        const requestId = req.params.requestId;
        try {
            // Get friend request object by id
            const friendRequest = await db.collection('friendrequests').findOne({ _id: new ObjectId(requestId) });
            if (!friendRequest) {
                return res.status(404).json({ error: 'Friend request not found' });
            }
            console.log("found request to accept:", friendRequest);
            // Update friends lists redundantly
            await Promise.all([
                db.collection('users').updateOne(
                    { _id: friendRequest.sender },
                    { $addToSet: { friends: friendRequest.receiver } } // Add receiver to sender's friends
                ),
                db.collection('users').updateOne(
                    { _id: friendRequest.receiver },
                    { $addToSet: { friends: friendRequest.sender } } // Add sender to receiver's friends
                )
            ]);
            console.log("accepted request, deleting from requests collection");
            // Delete the fulfilled request
            await db.collection('friendrequests').deleteOne({ _id: new ObjectId(requestId) });
            res.json({ message: 'Friend request accepted successfully' });
        } catch (err) {
            console.error('Error accepting friend request:', err);
            res.status(500).json({ error: 'Failed to accept friend request' });
        }
    });
    return router;
}

export default friendRouter;  // Export the friend router

