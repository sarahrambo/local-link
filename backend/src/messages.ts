import express, { Request, Response } from 'express';
import { Db, FindOneAndUpdateOptions, ObjectId } from 'mongodb';

// Function to check if a chat exists between two users and return its _id
export async function getOrCreateChatId(db: Db, user1Id: string, user2Id: string) {
    // Check if chat exists already
    const existingChat = await db.collection('chats').findOne({
        $or: [
            { user1: new ObjectId(user1Id), user2: new ObjectId(user2Id) },
            { user1: new ObjectId(user2Id), user2: new ObjectId(user1Id) }
        ]
    });
    // Return chat id
    if (existingChat) {
        console.log("found existing chat in db", existingChat._id);
        return existingChat._id;
    }
    // Chat doesn't exist, create new one
    const newChat = {
        user1: new ObjectId(user1Id),
        user2: new ObjectId(user2Id),
        messages: []
    };
    // Add to database
    const result = await db.collection('chats').insertOne(newChat);
    console.log("created new chat in db", result.insertedId);
    // Return the _id of new chat object
    return result.insertedId;
}

const messagesRouter = (db: Db) => {
    const router = express.Router();
    // Persistent messaging (between friends)
    // const ChatSchema = {
    //     user1: { type: ObjectId, required: true },
    //     user2: { type: ObjectId, required: true },
    //     messages: [
    //         {
    //             content: { type: String, required: true },
    //             timestamp: { type: Date, default: Date.now },
    //             senderId: { type: ObjectId, required: true },
    //             type: { type: String, required: true }
    //         }
    //     ]
    // };


    router.get('/ensurechat/:user1Id/:user2Id', async (req: Request, res: Response) => {
        getOrCreateChatId(db, req.params.user1Id, req.params.user2Id)
            .then(chatId => {
                console.log('Chat ID:', chatId);
                res.status(200).json({ chatId });
            })
            .catch(err => {
                console.error('Error:', err);
                res.status(500).json({ message: "failed to find or create chat" });
            });
    });
    router.get('/:userId/:friendId', async (req, res) => {
        const { userId, friendId } = req.params;

        try {
            const chat = await db.collection('chats').findOne({
                $or: [
                    { user1: new ObjectId(userId), user2: new ObjectId(friendId) },
                    { user1: new ObjectId(friendId), user2: new ObjectId(userId) }
                ]
            });

            if (!chat) {
                return res.status(404).json({ error: 'Chat not found' });
            }

            res.json(chat.messages);
        } catch (error) {
            console.error('Error fetching chat:', error);
            res.status(500).json({ error: 'Failed to fetch chat' });
        }
    });

    router.post('/', async (req: Request, res: Response) => {
        const { userId, friendId, content } = req.body;

        try {
            const chat = await db.collection('chats').findOneAndUpdate(
                {
                    $or: [
                        { user1: new ObjectId(userId), user2: new ObjectId(friendId) },
                        { user1: new ObjectId(friendId), user2: new ObjectId(userId) }
                    ]
                },
                {
                    $push: {
                        messages: {
                            content,
                            timestamp: new Date(),
                            senderId: new ObjectId(userId),
                            type: "text",
                        } as unknown as never // Explicitly define the type here
                    }
                },
                { upsert: true, returnDocument: 'after' } as FindOneAndUpdateOptions & { includeResultMetadata: true } // Explicitly define the options type
            );

            res.json(chat.value.messages);
        } catch (error) {
            console.error('Error sending message:', error);
            res.status(500).json({ error: 'Failed to send message' });
        }
    });

    // router.post('/', async (req: Request, res: Response) => {
    //     const { userId, friendId, content } = req.body;
    //
    //     try {
    //         const chat = await db.collection('chats').findOneAndUpdate(
    //             {
    //                 $or: [
    //                     { user1: new ObjectId(userId), user2: new ObjectId(friendId) },
    //                     { user1: new ObjectId(friendId), user2: new ObjectId(userId) }
    //                 ]
    //             },
    //             {
    //                 $push: {
    //                     messages: {
    //                         content,
    //                         timestamp: new Date(),
    //                         senderId: new ObjectId(userId),
    //                         type: "text",
    //                     }
    //                 }
    //             },
    //             { upsert: true, returnDocument: 'after' }
    //         );
    //
    //         res.json(chat.value.messages);
    //     } catch (error) {
    //         console.error('Error sending message:', error);
    //         res.status(500).json({ error: 'Failed to send message' });
    //     }
    // });
    return router;
}

export { messagesRouter };
