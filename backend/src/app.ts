import { connectToDb, getDb } from './db';
import { ObjectId } from 'mongodb';
import dotenv from 'dotenv';
import express, { Request, Response } from 'express';
import http from 'http';
import cors from 'cors';
import { Server } from 'socket.io';
import { verifyToken } from './jwt';
import accountRouter from './account';
import friendRouter from './friend';
// import { imageRouter } from './image';
import { messagesRouter, getOrCreateChatId } from './messages';
import { errorHandler } from './errorHandler';
import multer from 'multer';
import { Storage as CloudStorage } from '@google-cloud/storage';
import Redis from 'ioredis';
// const uploadImage = require('./image')
dotenv.config();

const app = express();
app.use(express.json());

interface MulterRequest extends Request {
    file: Express.Multer.File;
}
const upload = multer({ storage: multer.memoryStorage() });
const storage = new CloudStorage();
const bucketName = 'locallink-images'; // Replace with your bucket name
app.post('/uploads', upload.single('file'), async (req: MulterRequest, res) => {
    console.log("uploads endpoint");
    if (!req.file) {
        console.log("no file uploaded");
        res.status(400).json({ message: 'No file uploaded.' });
        return;
    }
    const bucket = storage.bucket(bucketName);
    const blob = bucket.file(Date.now() + "_" + req.file.originalname);
    const blobStream = blob.createWriteStream({
        resumable: false,
    });
    blobStream.on('error', err => {
        console.error(err);
        res.status(500).json({ message: 'Error uploading to Google Cloud Storage' });
    });
    blobStream.on('finish', async () => {
        await blob.makePublic();
        const publicUrl = `https://storage.googleapis.com/${bucketName}/${blob.name}`;
        res.status(200).json({url: publicUrl});
    });
    blobStream.end(req.file.buffer);
});


const server = http.createServer(app);
const io = new Server(server);

// Middleware
app.use(errorHandler); // catch and handle any uncaught errors thrown in routes/middleware

// Allow requests from specific origin (dashboard hostname)
const corsOptions = {
    origin: 'https://locallinkd.discovery.cs.vt.edu'
};
app.use(cors(corsOptions));

const port = process.env.PORT || 3001;

// connect to db
let db
connectToDb((err) => {
    if (!err) {
        db = getDb();
        app.use('/', accountRouter(db));
        app.use('/friend', friendRouter(db));
        app.use('/messages', messagesRouter(db));
        // app.use('/image', imageRouter(db));
        startServer();
    }
});

// Start the Express server
function startServer() {
    server.listen(port, () => {
        console.log(`Server is running on port ${port}`);
    });
}

// maps active socket IDs to user ids
// key: socket id, value: user id
const users = {};
// active socket IDs to socket object
// key: socket id, value: socket
const clients = {};


// ---------------- Matchmaking pool ---------------- 
// Create a Redis client
const redis = new Redis({
    host: "redis",
    port: 6379,
    // user: "root",
    // password: ""
});

// Handle errors
redis.on('error', (err) => {
    console.error('Redis error:', err);
});

async function clearMatchmakingPool() {
    // Create a readable stream (object mode)
    const stream = redis.scanStream({ match: 'matchmaking_pool:*' });
    stream.on('data', function (keys) {
        // `keys` is an array of strings representing key names
        if (keys.length) {
            const pipeline = redis.pipeline();
            keys.forEach(function (key) {
                pipeline.del(key);
            });
            pipeline.exec();
        }
    });
    stream.on('end', function () {
        console.log('Done clearing the pool.');
    });
}

// TODO: deal with issue of one person trying to log in from two different devices at once
// Function to pair users and start a chat session
async function pairUsersAndStartChat() {
    try {
        // Get two random users from the matchmaking pool
        const users = await redis.spop('matchmaking_pool', 2);
        if (users.length < 2) {
            console.log('Not enough users available for pairing');
            return;
        }

        const user1 = JSON.parse(users[0]);
        const user2 = JSON.parse(users[1]);
        const chatRoomName = `${user1.sid}-${user2.sid}`;

        // Emit event to clients indicating start of the chat session
        io.to(user1.sid).emit('chatStarted', { roomId: chatRoomName, partner: user2 });
        io.to(user2.sid).emit('chatStarted', { roomId: chatRoomName, partner: user1 });

        console.log('Users paired and chat started:', user1, user2);
    } catch (error) {
        console.error('Error pairing users and starting chat:', error);
    }
}

// Define matchmaking user object type
interface MatchmakingUser {
    sid: string;
    uid: string;
    name: string;
}

// Add a user object to the pool of active matchmaking users
async function addUserToMatchmakingPool(user: MatchmakingUser) {
    try {
        // Convert the user object to JSON string
        const userJson = JSON.stringify(user);
        // Check if already exists in pool
        const isMember = await redis.sismember('matchmaking_pool', JSON.stringify(user));

        // If the user is already present, do nothing
        if (isMember) {
            console.log('User is already in the matchmaking pool:', user);
            return;
        }

        // If the user is not present, add them to the pool
        await redis.sadd('matchmaking_pool', userJson);
        console.log('User added to matchmaking pool:', user);
        redis.scard('matchmaking_pool')
            .then((size) => {
                if (size >= 2) {
                    console.log(`Set has at least two entries`);
                    pairUsersAndStartChat();
                } else {
                    console.log(`Set does not have enough entries`);
                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    } catch (error) {
        console.error('Error adding user to matchmaking pool:', error);
    }
}



// // Remove a user object from the pool of active matchmaking users
// async function removeUserFromMatchmakingPool(user) {
//     try {
//         // Convert the user object to JSON string
//         const userJson = JSON.stringify(user);
//         await redis.srem('matchmaking_pool', userJson);
//         console.log('User removed from matchmaking pool:', user);
//     } catch (error) {
//         console.error('Error removing user from matchmaking pool:', error);
//     }
// }



// socket connection
io.on("connection", (socket) => {
    console.log('a user connected: ', socket.id);
    clients[socket.id] = socket; // add the client data to the hash

    socket.on('active-users', (userInfo) => {
        const userObj = { uid: userInfo.uid, name: userInfo.name, sid: socket.id };
        // add user to active users hash in redis
        redis.hset('active_users', socket.id, JSON.stringify(userInfo), (err, reply) => {
            if (err) {
                console.error('Error adding user to active users:', err);
            } else {
                console.log('User added to active users:', userObj);
            }
        });
        // set active status to true in mongodb
        try {
            db.collection('users')
                .updateOne(
                    { _id: new ObjectId(userInfo.uid) },
                    { $set: { active: true } }
                )
        }
        catch (error) {
            console.error('Error setting user active status:', error);
        }

    });

    socket.on('join-matchmaking', (joinInfo) => {
        console.log('user joined matchmaking: ', socket.id, joinInfo._id, joinInfo.name);
        // Remove from matchmaking pool
        addUserToMatchmakingPool({ sid: socket.id, uid: joinInfo._id, name: joinInfo.name });
    });

    // add user to users object to keep track
    socket.on('new-user', (name) => {
        users[socket.id] = name;
        console.log("new user added: " + name)
        console.log(users)
    });

    socket.on('start-chat', async (chatInfo) => {
        console.log("start-chat called", chatInfo);
        // let startedChatId = "";
        getOrCreateChatId(db, chatInfo.selfid, chatInfo.friendid)
            .then(async (chatId) => {
                // startedChatId = chatId;
                const chatObj = await db.collection('chats')
                    .findOne({ _id: new ObjectId(chatId) });
                console.log("sending load-messages", chatObj._id);
                socket.emit("load-messages", chatObj);
            })
            .catch(err => {
                console.error('Error:', err);
            });

    });
    socket.on('request-messages', async (chatInfo) => {
        console.log('request-messages called');
        if(chatInfo.chatId !== undefined && chatInfo.chatId!=="") {
            try {
                const chatObj = await db.collection('chats')
                    .findOne({ _id: new ObjectId(chatInfo.chatId) });
                console.log("resending load-messages", chatObj._id);
                socket.emit("load-messages", chatObj);
            } catch (err) {
                console.log("request-messages error: ", err);
            }
        } else {
            console.log("no chat obj id provided");
        }
    });

    socket.on('sendPMessage', async ({messageContent, chatid, userId, type }) => {
        // TODO: check if partner is connected, emit message to their chat for update
        await db.collection('chats').findOneAndUpdate(
            { _id: new ObjectId(chatid) },
            {
                $push: {
                    messages: {
                        messageContent,
                        timestamp: new Date(),
                        senderId: new ObjectId(userId),
                        type,
                    }
                }
            },
            { upsert: false, returnDocument: 'after' }
        );
    });
    // listen for messages
    socket.on('sendMessage', ({messageContent, partnerSocketId, type}) => {
        console.log(`socketid: ${socket.id}, Message: ${messageContent}, partnerid: ${partnerSocketId}, type: ${type}`);
        // Find socket associated with the partnerSocketId
        const recipientSocket = clients[partnerSocketId];

        if (recipientSocket) {
            // Emit the message to the recipient
            recipientSocket.emit('receiveMessage', { type, messageContent, timestamp: new Date().toISOString() });
        } else {
            // Handle if recipient is not connected
            console.log('Recipient is not connected');
        }
    });

    socket.on("disconnect", () => {
        // remove user from users object
        delete users[socket.id];
        delete clients[socket.id];
        console.log('a user disconnected')
        // get user info from active users hash in redis
        redis.hget('active_users', socket.id, (err, reply) => {
            if (err) {
                console.error('Error fetching user info from Redis:', err);
            } else {
                const userInfo = JSON.parse(reply);
                // set active status to false in mongodb
                try {
                    db.collection('users')
                        .updateOne(
                            { _id: new ObjectId(userInfo.uid) },
                            { $set: { active: false } }
                        )
                }
                catch (error) {
                    console.error('Error setting user active status:', error);
                }
            }
        });
        // remove user from active users hash in redis
        redis.hdel('active_users', socket.id, (err, reply) => {
            if (err) {
                console.error('Error removing user from active users:', err);
            } else {
                console.log('User removed from active users:', socket.id);
            }
        });
        // TODO: remove user from matchmaking pool, if in it
    })
});

// TODO: place in proper routes file, add authentication
app.get('/mpoolsize', (req: Request, res: Response) => {
    redis.scard('matchmaking_pool')
        .then((size) => {
            console.log("sending mpool size");
            res.status(200).json({ nUsers: size });
        })
        .catch((error) => {
            console.log(error);
            res.status(500).json({ message: "Failed to read redis matchmaking pool size" });
        });
});

app.post('/clearmpool', async () => {
    console.log("clearing matchmaking user pool");
    clearMatchmakingPool();
});

app.post('/clear-active-users', async () => {
    console.log("clearing active user set");
    redis.del('active_users')
});

app.get('/active-users-set', (req: Request, res: Response) => {
    redis.hgetall('active_users', (err, members) => {
        if (err) {
            console.error('Error fetching active users from Redis:', err);
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            res.json({ activeUsers: members });
        }
    });
});

app.get('/active-users-count', (req: Request, res: Response) => {
    redis.hlen('active_users', (err, count) => {
        if (err) {
            console.error('Error fetching active users count from Redis:', err);
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            res.json({ activeUsersCount: count });
        }
    });
});

// gets all users in the database
app.get('/all-users', (req: Request, res: Response) => {
    const arr = [];
    db.collection('users')
        .find()
        .forEach((user) => {
            arr.push({_id: user._id, name: user.name, profile_uri: user.profile_uri, active: true });
        })
        .then(() => {
            res.json(arr);
        })
});

// gets all active users
// Secured route using JWT middleware function; only proceeds if valid token is provided
app.get('/active-users', verifyToken, async (req: Request, res: Response) => {
    redis.hgetall('active_users', async (err, members: Record<string, string>) => {
        if (err) {
            console.error('Error fetching active users from Redis:', err);
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            console.log("getting active users")
            // Extract uids
            const uids = Object.values(members).map(member => new ObjectId(JSON.parse(member).uid));
            // Query MongoDB based on _id
            const query = { _id: { $in: uids } };
            const arr = [];
            const result = await db.collection('users').find(query)
                .forEach((user) => {
                    arr.push({_id: user._id, name: user.name, profile_uri: user.profile_uri, active: user.active });
                })
                .then(() => {
                    res.json({ activeUsers: arr });
                });
        }
    });
});

app.get('/test', (req: Request, res: Response) => {
    res.json({ message: "howdy" });
});

app.get('/users/:userId/profile_uri', async (req: Request, res: Response) => {
    const userId = req.params.userId;
    console.log("get profile endpoint", userId);
    try {
        const user = await db.collection('users')
            .findOne({ _id: new ObjectId(userId) });

        if (!user) {
            res.status(404).json({ error: 'User not found' });
            return;
        }
        const found_uri = user.profile_uri || "";
        res.status(200).json({ profile_uri: found_uri });
    } catch (error) {
        console.error('Error fetching profile_uri:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});
app.get('/users/:userId/friends', async (req: Request, res: Response) => {
    const userId = req.params.userId;
    try {
        const userIdObj = new ObjectId(userId);
        const friendsPipeline = [
            // Find relevant user
            { 
                $match: { _id: userIdObj } 
            },
            {
                // get the ids of all friends 
                $lookup: {
                    from: "users",
                    localField: "friends",
                    foreignField: "_id",
                    as: "friends"
                }
            },
            // Unpack list of friend ids
            {
                $unwind: "$friends"
            },
            {
                $replaceRoot: { newRoot: "$friends" }
            },
            {
                $addFields: {
                    profile_uri: { $ifNull: ["$profile_uri", ""] }
                }
            },
            // remove unnecessary fields
            {
                $project: { password: 0, friends: 0 }
            }
        ];
        const chatsPipeline = [
            {
                $match: {
                    $or: [
                        { user1: userIdObj },
                        { user2: userIdObj }
                    ]
                }
            },
            {
                $unwind: "$messages"
            },
            // Sort in descending order
            {
                $sort: { "messages.timestamp": -1 }
            },
            {
                $group: {
                    _id: {
                        $cond: [
                            { $eq: ["$user1", userIdObj] },
                            "$user2",
                            "$user1"
                        ]
                    },
                    lastMessage: { $first: "$messages" }
                }
            },
            {
                $lookup: {
                    from: "users",
                    localField: "_id",
                    foreignField: "_id",
                    as: "friend"
                }
            },
            {
                $unwind: "$friend"
            },
            {
                $addFields: {
                    "friend.lastMessage": "$lastMessage"
                }
            },
            {
                $replaceRoot: { newRoot: "$friend" }
            }
        ];

        const friends = await db.collection('users').aggregate(friendsPipeline).toArray();
        const friendsWithLastMessage = await db.collection('chats').aggregate(chatsPipeline).toArray();
        const mergedFriends = friends.map(friend => {
            const friendWithLastMessage = friendsWithLastMessage.find(f => f._id.equals(friend._id));
            if (friendWithLastMessage) {
                friend.lastMessage = friendWithLastMessage.lastMessage;
            } else {
                friend.lastMessage = null;
            }
            return friend;
        });

        res.json(mergedFriends);


        // const user = await db.collection('users')
        //     .findOne({ _id: new ObjectId(userId) });
        //
        // if (!user) {
        //     res.status(404).json({ error: 'User not found' });
        //     return;
        // }
        //
        // // Assuming friends are stored as an array of ObjectIds in the user document:
        // const friendIds = user.friends || []; 
        //
        // const friends = await db.collection('users').find({ _id: { $in: friendIds } }).toArray();
        // const parsedFriends = friends.map(obj => {
        //     // Ensure list of fields are present in friend objects
        //     if (!('profile_uri' in obj)) {
        //         obj['profile_uri'] = "";
        //     }
        //     obj.active = true;
        //     // Remove list of unneeded fields from the object
        //     ['password', 'friends'].forEach(field => {
        //         delete obj[field];
        //     });
        //     return obj;
        // });
        // res.json(parsedFriends);
    } catch (error) {
        console.error('Error fetching friends:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.put('/users/:userId/profile_uri', async (req: Request, res: Response) => {
    const userId = req.params.userId;
    const profileUri = req.body.profile_uri;

    try {
        const result = await db.collection('users').updateOne(
            { _id: new ObjectId(userId) },
            { $set: { profile_uri: profileUri } }
        );

        if (result.matchedCount === 0) {
            res.status(404).json({ error: 'User not found' });
            return;
        }

        res.json({ message: 'Profile URI updated successfully' });
    } catch (error) {
        console.error('Error updating profile URI:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.get('/users/:userId/profile_data', async (req: Request, res: Response) => {
    const userId = req.params.userId;
    try {
        const user = await db.collection('users')
            .findOne({ _id: new ObjectId(userId) });

        if (!user) {
            res.status(404).json({ error: 'User not found' });
            return;
        }
        const profile_data = user.profile_data || {};

        res.status(200).json({ profile_data: profile_data });
    } catch (error) {
        console.error('Error fetching profile_data:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.put('/users/:userId/profile_data', async (req: Request, res: Response) => {
    const userId = req.params.userId;
    const profileData = req.body;

    try {
        const result = await db.collection('users').updateOne(
            { _id: new ObjectId(userId) },
            { $set: { profile_data: profileData } }
        );
        if (result.matchedCount === 0) {
            res.status(404).json({ error: 'User not found' });
            return;
        }
        res.json({ message: 'Profile data updated successfully' });
    } catch (error) {
        console.error('Error updating profile data:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.post('/update-location', async (req: Request, res: Response) => {
    db.collection('users')
        .updateOne(
            { _id: new ObjectId(req.body.userId) },
            { $set: { location: {latitude: req.body.latitude, longitude: req.body.longitude, time: new Date().toISOString()} } }
        )
        .then(() => {
            res.json({ message: "Location updated" });
        })
        .catch((err) => {
            console.error(err);
            res.status(500).json({ message: "Failed to update location" });
        });
});

app.get('/get-locations', async (req: Request, res: Response) => {
    const locations = [];
    db.collection('users')
        .find()
        .forEach((user) => {
            locations.push({_id: user._id, name : user.name, location: user.location, profile_uri: user.profile_uri});
        })
        .then(() => {
            res.json(locations);
        })
        .catch((err) => {
            console.error(err);
            res.status(500).json({ message: "Failed to get locations" });
        });
});

// get all (active) users within a certain radius of a location, excluding the requesting user
app.post('/get-nearby-users', async (req: Request, res: Response) => {
    const userId = req.body.userId;
    const radius = req.body.radius;
    const lat = req.body.latitude;
    const lon = req.body.longitude;
    const locations = [];
    const requireActive = req.body.requireActive || false;
    db.collection('users')
        .find()
        .forEach((user) => {
            if (user.location) {
                const userLat = user.location.latitude;
                const userLon = user.location.longitude;
                const distance = getDistance(lat, lon, userLat, userLon);
                if (distance <= radius && (!requireActive || user.active) && userId !== user._id.toString()){
                    locations.push(user);
                }
            }
        })
        .then(() => {
            res.json(locations);
        })
        .catch((err) => {
            console.error(err);
            res.status(500).json({ message: "Failed to get nearby users" });
        });
});

// distance in miles between two latitude/longitude pairs
function getDistance(lat1, lon1, lat2, lon2) {
    const earthRadiusMiles = 3958.8; // Radius of the Earth in miles
    const dLat = deg2rad(lat2 - lat1);
    const dLon = deg2rad(lon2 - lon1); 
    const a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
                Math.sin(dLon/2) * Math.sin(dLon/2); 
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    const distance = earthRadiusMiles * c; // Distance in miles
    return distance;
}

function deg2rad(deg) {
    return deg * (Math.PI/180);
}
