import dotenv from 'dotenv';
dotenv.config();
import { MongoClient } from 'mongodb';

let dbConnection
const uri = process.env.MONGODB_URI

export function connectToDb(cb) {
    MongoClient.connect(uri)
    .then((client) => {
        dbConnection = client.db()
        return cb()
    })
    .catch((err) => {
        console.error("Error connecting to db", err)
        return cb(err)
    });
}
export function getDb() {
    return dbConnection
}
