
// Info stored in json web token
export type JWT_Info = {
    _id: string;
    name: string;
};
// User record info, without _id
export type UserRecord = {
    name: string;
    password: string;
    profile_uri: string;
    friends: [];
};
