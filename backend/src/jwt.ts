import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import { JWT_Info } from './types';

// Middleware to verify JWT token
interface CustomRequest extends Request {
    userId?: string;
}

const verifyToken = (req: CustomRequest, res: Response, next: NextFunction) => {
    const token = req.headers.authorization?.split(' ')[1];
    if (!token) {
        return res.status(403).json({ message: 'Token not provided' });
    }

    jwt.verify(token, process.env.JWT_SECRET, (err: Error, decoded: JWT_Info) => {
        if (err) {
            return res.status(401).json({ message: 'Invalid token' });
        }
        console.log(decoded);
        // jwt verified, go to next middleware
        next();
    });
};

export { verifyToken };
