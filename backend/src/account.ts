import bcrypt from 'bcrypt';
import express, { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { Db } from 'mongodb';
import { UserRecord } from './types';

const accountRouter = (db: Db) => {
    const router = express.Router();
    router.post('/create-account', async (req: Request, res: Response) => {
        console.log('create endpoint');
        const newRecord = req.body;
        const hashedPassword: string = await bcrypt.hash(newRecord.password, 10);
        const newUser: UserRecord = {
            name: newRecord.name,
            password: hashedPassword,
            profile_uri: "",
            friends: [],
        };
        // Check if user already exists
        db.collection('users')
            .findOne({ name: newUser.name })
            .then((existingAccount) => {
                if (existingAccount === null) {
                    db.collection('users')
                        .insertOne(newUser)
                        .then((result: any) => {
                            const insertedId = result.insertedId;
                            // Fetch the newly inserted document by its _id
                            db.collection('users').findOne({ _id: insertedId }).then((result: any) => {
                                const jwt_token = jwt.sign({ _id: result._id, name: result.name }, process.env.JWT_SECRET);
                                res.json({ _id: result._id, name: result.name, jwt_token: jwt_token });
                                return result;
                            }).catch((err: Error) => {
                                    res.status(500).json(err);
                                });
                        })
                        .catch((err) => {
                            res.status(500).json(err);
                        });
                } else {
                    res.status(409).json({ error: 'User already exists' });
                }
            })
            .catch((err) => {
                res.status(500).json(err);
            });
    });

    router.post('/login', (req: Request, res: Response) => {
        console.log('login endpoint');
        const user = req.body;
        db.collection('users')
            .findOne({ name: user.name })
            .then((result) => {
                if (result === null) {
                    // console.log(`failed to find user ${user.name}`);
                    res.status(404).json({ error: 'User not found' });
                    return;
                } else {
                    try {
                        bcrypt.compare(user.password, result.password, function(err: Error, compres: boolean) {
                            if(err) {
                                console.log("a");
                                res.status(500).send({message: "Error comparing passwords" + err})
                            }
                            if (compres) {
                                const jwt_token = jwt.sign({ _id: result._id, name: result.name }, process.env.JWT_SECRET);
                                res.json({ _id: result._id, name: result.name, jwt_token: jwt_token });
                            } else {
                                res.status(401).json({ error: 'Password does not match' });
                            }
                        });
                    } catch (error) {
                        console.log("b");
                        res.status(500).send({message: "invalid password; encryption validate err;" + error});
                    }
                }
            })
            .catch((err) => {
                console.log("c");
                res.json(err);
            });
    });

    return router;
}

export default accountRouter;  // Export the account router
