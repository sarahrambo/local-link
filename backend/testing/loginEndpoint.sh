#!/usr/bin/bash

# Define the default endpoint URL
deployed_url="https://locallinkb.discovery.cs.vt.edu/"
endpoint_path="login"

# Check user input and set endpoint URL accordingly
if [ "$1" == "l" ]; then
    echo "entered l for local backend:"
    endpoint_url="http://localhost:3001/$endpoint_path"
else
    echo "accessing deployed backend:"
    endpoint_url="$deployed_url$endpoint_path"
fi
echo $endpoint_url
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"name":"Joey","password":"Joey"}' \
  $endpoint_url

