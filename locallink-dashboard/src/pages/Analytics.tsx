const Analytics = () => {
    return (
    <div className="rounded shadow bg-gray-300 p-5 h-min">
        <h1 className="mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900">Analytics</h1>
        <div className="p-3 rounded shadow-md shadow-gray-500 bg-indigo-200">
        <h2 className="text-4x1 font-bold">Project Links</h2>
        <ul>
            <li><a className="font-medium text-blue-600 dark:text-blue-500 hover:underline" href="https://cloud.cs.vt.edu">k8s Rancher UI</a></li>
            <li><a className="font-medium text-blue-600 dark:text-blue-500 hover:underline" href="https://git.cs.vt.edu/sarahrambo/local-link">Project Repository</a></li>
            <li><a className="font-medium text-blue-600 dark:text-blue-500 hover:underline" href="https://git.cs.vt.edu/sarahrambo/local-link/container_registry">Private Container Registry</a></li>
            <li><a className="font-medium text-blue-600 dark:text-blue-500 hover:underline" href="https://cloud.mongodb.com">MongoDB</a></li>
        </ul>
        </div>
    </div>
  );
};

export default Analytics;

