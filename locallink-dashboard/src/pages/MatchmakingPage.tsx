import React, { useState } from 'react';
import { BACKEND_URL } from '../constants';

const MatchmakingPage: React.FC = () => {
    const [numUsers, setNumUsers] = useState(0);

    const clearMPool = () => {
        fetch(`${BACKEND_URL}/clearmpool`, {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            // TODO: Add auth
            },
                body: JSON.stringify({ }),
            })
        .then(response => {
            if (!response.ok) {
                console.error(`response error`);
                console.error(response);
            } else {
                console.log('clear POST request sent successfully');
            }
          })
          .catch(error => {
            console.error('POST request failed:', error);
          });
    };

    const fetchMPoolSize = () => {
        fetch(`${BACKEND_URL}/mpoolsize`)
        .then(response => {
            if (!response.ok) {
                console.error(response);
            } else {
                return response.json();
             }   
          })
        .then((data) => {
                setNumUsers(data.nUsers);
            })
          .catch(error => {
            console.error('GET request failed:', error);
          });
    };

    return (
     <div className="flex flex-col gap-3 rounded shadow bg-gray-300 p-5 h-min">
        <h1 className="mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900">Matchmaking Pool</h1>
        <button onClick={clearMPool} className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow">Clear Pool</button>
        <div className="p-3 rounded shadow-md shadow-gray-500 bg-indigo-200">
        <p>{numUsers} users in mpool</p>
        <button onClick={fetchMPoolSize} className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow">Fetch Pool Size</button>
        </div>
    </div>
    );
};
export default MatchmakingPage;
