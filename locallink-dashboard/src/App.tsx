import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./pages/Layout";
import MatchmakingPage from "./pages/MatchmakingPage";
import Users from "./pages/Users";
import Analytics from "./pages/Analytics";

const App: React.FC = () => {
  return (
  <BrowserRouter basename="/">
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Users />} />
          <Route path="/matchmaking" element={<MatchmakingPage />} />
          <Route path="/analytics" element={<Analytics />} />
          <Route path="*" element={<Users />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App;

